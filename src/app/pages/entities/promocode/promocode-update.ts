import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Promocode } from './promocode.model';
import { PromocodeService } from './promocode.service';
import { User } from '../../../services/user/user.model';
import { UserService } from '../../../services/user/user.service';

@Component({
    selector: 'page-promocode-update',
    templateUrl: 'promocode-update.html'
})
export class PromocodeUpdatePage implements OnInit {

    promocode: Promocode;
    users: User[];
    createDate: string;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        code: [null, []],
        flagActive: ['false', []],
        name: [null, []],
        description: [null, []],
        containerClass: [null, []],
        containerId: [null, []],
        createDate: [null, []],
        user: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private userService: UserService,
        private promocodeService: PromocodeService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.userService.findAll().subscribe(data => this.users = data, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.promocode = response.data;
            this.isNew = this.promocode.id === null || this.promocode.id === undefined;
        });
    }

    updateForm(promocode: Promocode) {
        this.form.patchValue({
            id: promocode.id,
            code: promocode.code,
            flagActive: promocode.flagActive,
            name: promocode.name,
            description: promocode.description,
            containerClass: promocode.containerClass,
            containerId: promocode.containerId,
            createDate: (this.isNew) ? new Date().toISOString() : promocode.createDate,
            user: promocode.user,
        });
    }

    save() {
        this.isSaving = true;
        const promocode = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.promocodeService.update(promocode));
        } else {
            this.subscribeToSaveResponse(this.promocodeService.create(promocode));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Promocode>>) {
        result.subscribe((res: HttpResponse<Promocode>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `Promocode ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/promocode');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): Promocode {
        return {
            ...new Promocode(),
            id: this.form.get(['id']).value,
            code: this.form.get(['code']).value,
            flagActive: this.form.get(['flagActive']).value,
            name: this.form.get(['name']).value,
            description: this.form.get(['description']).value,
            containerClass: this.form.get(['containerClass']).value,
            containerId: this.form.get(['containerId']).value,
            createDate: new Date(this.form.get(['createDate']).value),
            user: this.form.get(['user']).value,
        };
    }

    compareUser(first: User, second: User): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}
