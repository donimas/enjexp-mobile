import {Component} from '@angular/core';
import {NavController, ToastController, Platform, IonItemSliding, Events} from '@ionic/angular';
import {filter, map} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';
import {JhiDataUtils} from 'ng-jhipster';
import {Menu} from './menu.model';
import {MenuService} from './menu.service';
import {ModalController} from '@ionic/angular';
import {AmountModal} from './amount.modal';
import {Category, CategoryService} from "../category";
import {Order, OrderService} from "../order";
import {OrderItem, OrderItemService} from "../order-item";
import {UserService} from "../../../services/user/user.service";
import {OpenApiService} from "../../../services/open-api/open-api.service";

@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html',
    styleUrls: ['menu.page.scss']
})
export class MenuPage {
    menus: Menu[];
    categories: Category[];
    groupedCategories = [];
    demo: Array<any> = [
        {name: 'Феттучини "Альфредо"', ingr: 'Куриное филе, сливки, пармезан, грибы, черри', price: '1500', time: '15'},
        {name: 'Карбонара', ingr: 'Говядина, лук, яйцо, сливки, пармезан', price: '1500', time: '15'},
        {
            name: 'Orange Chicken',
            ingr: 'Куриное филе, рисовый уксус, соя, имбирь, апельсин, чеснок, чили, кисло сладкий соус',
            price: '1300',
            time: '15'
        },
        {name: 'Рамен"', ingr: 'Лапша, куриное филе, шампиньоны, яйцо, кочулян, лук, черри', price: '900', time: '15'},
    ];

    // todo: add pagination

    slideOpts = {
        initialSlide: 0,
        slidesPerView: 4,
        loop: false,
        centeredSlides: false
    };

    constructor(
        private dataUtils: JhiDataUtils,
        private navController: NavController,
        private menuService: MenuService,
        private toastCtrl: ToastController,
        private modalController: ModalController,
        private categoryService: CategoryService,
        private orderItemService: OrderItemService,
        private events: Events,
        private userService: UserService,
        private openApiService: OpenApiService,
        public plt: Platform
    ) {
        this.menus = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        //this.menuService.query().pipe(
        this.openApiService.findAllMenu().pipe(
            filter((res: HttpResponse<Menu[]>) => res.ok),
            map((res: HttpResponse<Menu[]>) => res.body)
        )
            .subscribe(
                (response: Menu[]) => {
                    this.menus = response;
                    console.log(this.menus);

                    this.loadCategories();

                    if (typeof (refresher) !== 'undefined') {
                        setTimeout(() => {
                            refresher.target.complete();
                        }, 750);
                    }
                },
                async (error) => {
                    console.error(error);
                    const toast = await this.toastCtrl.create({
                        message: 'Failed to load data',
                        duration: 2000,
                        position: 'middle'
                    });
                    toast.present();
                });
    }

    async loadCategories(refresher?) {
        //this.categoryService.query().pipe(
        this.openApiService.findAllCategories().pipe(
            filter((res: HttpResponse<Category[]>) => res.ok),
            map((res: HttpResponse<Category[]>) => res.body)
        )
            .subscribe(
                (response: Category[]) => {
                    this.categories = response;
                    if(this.menus && this.categories) {
                        this.groupByCategory();
                    }

                    if (typeof(refresher) !== 'undefined') {
                        setTimeout(() => {
                            refresher.target.complete();
                        }, 750);
                    }
                },
                async (error) => {
                    console.error(error);
                    const toast = await this.toastCtrl.create({message: 'Failed to load categories', duration: 2000, position: 'middle'});
                    toast.present();
                });
    }

    groupByCategory() {
        this.categories.forEach((c) => {
            let menuItems: Menu[] = [];
            let category = {
                id: c.id,
                name: c.name,
                items: menuItems
            };
            this.menus.forEach((m) => {
                if(c.id === m.category.id) {
                    category.items.push(m);
                }
            });

            this.groupedCategories.push(category);
        });

        console.log('categories grouped', this.groupedCategories);
    }

    trackId(index: number, item: Menu) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    new() {
        this.navController.navigateForward('/tabs/entities/menu/new');
    }

    edit(item: IonItemSliding, menu: Menu) {
        this.navController.navigateForward('/tabs/entities/menu/' + menu.id + '/edit');
        item.close();
    }

    async delete(menu) {
        this.menuService.delete(menu.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'Menu deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(menu?: Menu) {
        console.log(menu);
        //this.navController.navigateForward('/tabs/entities/menu/' + menu.id + '/view');
        this.navController.navigateForward('/tabs/home/regular-menu/' + menu.id + '/view');
    }

    async selectAmount(e) {
        if(!e) {
            return;
        }
        console.log('order item:', e);
        this.userService.addCheckout(e);
        this.events.publish('checkout:amount', e);
    }

    async logScrolling(e) {
        console.log(e);
        /*const content = document.querySelector('ion-content');
        const scrollElement = await content.getScrollElement();
        console.log(scrollElement);*/
        /*content.getScrollElement().then((el) => {
        });*/
    }
}
