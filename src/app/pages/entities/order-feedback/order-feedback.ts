import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { OrderFeedback } from './order-feedback.model';
import { OrderFeedbackService } from './order-feedback.service';

@Component({
    selector: 'page-order-feedback',
    templateUrl: 'order-feedback.html'
})
export class OrderFeedbackPage {
    orderFeedbacks: OrderFeedback[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private orderFeedbackService: OrderFeedbackService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.orderFeedbacks = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.orderFeedbackService.query().pipe(
            filter((res: HttpResponse<OrderFeedback[]>) => res.ok),
            map((res: HttpResponse<OrderFeedback[]>) => res.body)
        )
        .subscribe(
            (response: OrderFeedback[]) => {
                this.orderFeedbacks = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: OrderFeedback) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/order-feedback/new');
    }

    edit(item: IonItemSliding, orderFeedback: OrderFeedback) {
        this.navController.navigateForward('/tabs/entities/order-feedback/' + orderFeedback.id + '/edit');
        item.close();
    }

    async delete(orderFeedback) {
        this.orderFeedbackService.delete(orderFeedback.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'OrderFeedback deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(orderFeedback: OrderFeedback) {
        this.navController.navigateForward('/tabs/entities/order-feedback/' + orderFeedback.id + '/view');
    }
}
