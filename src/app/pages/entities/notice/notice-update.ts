import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Notice } from './notice.model';
import { NoticeService } from './notice.service';

@Component({
    selector: 'page-notice-update',
    templateUrl: 'notice-update.html'
})
export class NoticeUpdatePage implements OnInit {

    notice: Notice;
    createDate: string;
    modifyDate: string;
    sentTime: string;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        title: [null, []],
        content: [null, []],
        smsContent: [null, []],
        flagPush: ['false', []],
        flagSms: ['false', []],
        flagEmail: ['false', []],
        receiverType: [null, []],
        createDate: [null, []],
        modifyDate: [null, []],
        containerClass: [null, []],
        containerId: [null, []],
        noticeType: [null, []],
        flagSent: ['false', []],
        sentTime: [null, []],
        payloadObjectUrl: [null, []],
        payloadObjectId: [null, []],
        flagDeleted: ['false', []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private noticeService: NoticeService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.notice = response.data;
            this.isNew = this.notice.id === null || this.notice.id === undefined;
        });
    }

    updateForm(notice: Notice) {
        this.form.patchValue({
            id: notice.id,
            title: notice.title,
            content: notice.content,
            smsContent: notice.smsContent,
            flagPush: notice.flagPush,
            flagSms: notice.flagSms,
            flagEmail: notice.flagEmail,
            receiverType: notice.receiverType,
            createDate: (this.isNew) ? new Date().toISOString() : notice.createDate,
            modifyDate: (this.isNew) ? new Date().toISOString() : notice.modifyDate,
            containerClass: notice.containerClass,
            containerId: notice.containerId,
            noticeType: notice.noticeType,
            flagSent: notice.flagSent,
            sentTime: (this.isNew) ? new Date().toISOString() : notice.sentTime,
            payloadObjectUrl: notice.payloadObjectUrl,
            payloadObjectId: notice.payloadObjectId,
            flagDeleted: notice.flagDeleted,
        });
    }

    save() {
        this.isSaving = true;
        const notice = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.noticeService.update(notice));
        } else {
            this.subscribeToSaveResponse(this.noticeService.create(notice));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Notice>>) {
        result.subscribe((res: HttpResponse<Notice>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `Notice ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/notice');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): Notice {
        return {
            ...new Notice(),
            id: this.form.get(['id']).value,
            title: this.form.get(['title']).value,
            content: this.form.get(['content']).value,
            smsContent: this.form.get(['smsContent']).value,
            flagPush: this.form.get(['flagPush']).value,
            flagSms: this.form.get(['flagSms']).value,
            flagEmail: this.form.get(['flagEmail']).value,
            receiverType: this.form.get(['receiverType']).value,
            createDate: new Date(this.form.get(['createDate']).value),
            modifyDate: new Date(this.form.get(['modifyDate']).value),
            containerClass: this.form.get(['containerClass']).value,
            containerId: this.form.get(['containerId']).value,
            noticeType: this.form.get(['noticeType']).value,
            flagSent: this.form.get(['flagSent']).value,
            sentTime: new Date(this.form.get(['sentTime']).value),
            payloadObjectUrl: this.form.get(['payloadObjectUrl']).value,
            payloadObjectId: this.form.get(['payloadObjectId']).value,
            flagDeleted: this.form.get(['flagDeleted']).value,
        };
    }

}
