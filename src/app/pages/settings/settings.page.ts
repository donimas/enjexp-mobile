import {Component, OnInit} from '@angular/core';
import {AlertController, Events, NavController, Platform, ToastController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from 'src/app/services/login/login.service';
import {FormBuilder} from "@angular/forms";
import {OrderItem} from "../entities/order-item";
import {AccountService} from "../../services/auth/account.service";
import {UserService} from "../../services/user/user.service";
import {ClientApiService} from "../../services/client-api/client-api.service";
import {SmsConfirmModal} from "../entities/modals/sms-confirm.modal";
import {Order} from "../entities/order";

@Component({
    selector: 'settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss']
})
export class SettingsPage implements OnInit {

    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;
    account: {
        login: string;
        email: string;
        firstName: string;
        langKey: string;
        noticeConfirm: boolean;
    } = {
        login: '',
        email: '',
        firstName: '',
        langKey: 'ru',
        noticeConfirm: false
    };

    constructor(
        public translateService: TranslateService,
        public toastController: ToastController,
        public navController: NavController,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private clientApiService: ClientApiService,
        private accountService: AccountService,
        private alertController: AlertController,
        public platform: Platform,
        private events: Events
    ) {
    }

    ngOnInit() {
        this.accountService.identity().then((account) => {
            this.account.firstName = account.firstName;
            this.account.login = account.login;
            this.account.email = account.email;
            this.account.langKey = account.langKey;
            this.account.noticeConfirm = account.noticeConfirm;
        });
    }

    doUpdate() {
        this.userService.updateClient(this.account).subscribe((resp) => {
            console.log(resp);
            this.accountService.identity(true).then((account) => {
                if(account) {
                    this.clientApiService.showMsg('Запись успешно изменена');
                } else {
                    this.clientApiService.showMsg('Ошибка при изменении. Позже попробуйте еще раз');
                }
            }, (err) => {
                this.clientApiService.showMsg('Ошибка при изменении. Позже попробуйте еще раз');
            });

            this.navController.back();
        }, (err) => {
            this.clientApiService.showFLCError(err);
        });
    }

    logout() {
        this.userService.logout();
        //this.navController.back();
        //this.navController.back({animated: true});
        this.navController.navigateBack('/tabs/home', {animated: true});
        this.events.publish('account:refresh');
    }

    async logoutConfirm() {
        const alert = await this.alertController.create({
            header: 'Подтверждение',
            message: 'Вы действительно хотите выйти?',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Да',
                    handler: () => {
                        this.logout();
                    }
                }
            ]
        });
        await alert.present();
    }
}
