import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Notice } from './notice.model';

@Injectable({ providedIn: 'root'})
export class NoticeService {
    private resourceUrl = ApiService.API_URL + '/notices';

    constructor(protected http: HttpClient) { }

    create(notice: Notice): Observable<HttpResponse<Notice>> {
        return this.http.post<Notice>(this.resourceUrl, notice, { observe: 'response'});
    }

    update(notice: Notice): Observable<HttpResponse<Notice>> {
        return this.http.put(this.resourceUrl, notice, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<Notice>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<Notice[]>> {
        const options = createRequestOption(req);
        return this.http.get<Notice[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
