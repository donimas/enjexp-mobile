import { BaseEntity } from 'src/model/base-entity';
import { User } from '../../../services/user/user.model';

export class Promocode implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public flagActive?: boolean,
        public name?: string,
        public description?: string,
        public containerClass?: string,
        public containerId?: number,
        public createDate?: any,
        public user?: User,
    ) {
        this.flagActive = false;
    }
}
