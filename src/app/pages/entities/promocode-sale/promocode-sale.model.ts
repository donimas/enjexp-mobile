import { BaseEntity } from 'src/model/base-entity';
import { Promocode } from '../promocode/promocode.model';

export class PromocodeSale implements BaseEntity {
    constructor(
        public id?: number,
        public sale?: number,
        public minPrice?: number,
        public maxPrice?: number,
        public minAmount?: number,
        public maxAmount?: number,
        public minShare?: number,
        public startDate?: any,
        public endDate?: any,
        public promocode?: Promocode,
    ) {
    }
}
