import { Component, OnInit } from '@angular/core';
import { NoticeReceiver } from './notice-receiver.model';
import { NoticeReceiverService } from './notice-receiver.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-notice-receiver-detail',
    templateUrl: 'notice-receiver-detail.html'
})
export class NoticeReceiverDetailPage implements OnInit {
    noticeReceiver: NoticeReceiver = {};

    constructor(
        private navController: NavController,
        private noticeReceiverService: NoticeReceiverService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.noticeReceiver = response.data;
        });
    }

    open(item: NoticeReceiver) {
        this.navController.navigateForward('/tabs/entities/notice-receiver/' + item.id + '/edit');
    }

    async deleteModal(item: NoticeReceiver) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.noticeReceiverService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/notice-receiver');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
