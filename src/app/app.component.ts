import {Component} from '@angular/core';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Platform} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {OneSignal} from "@ionic-native/onesignal/ngx";
import {ClientApiService} from "./services/client-api/client-api.service";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translate: TranslateService,
        private oneSignal: OneSignal,
        private clientApiService: ClientApiService,
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            if(this.platform.is('cordova')) {
                this.setupPush();
            }
        });
        this.initTranslate();
    }

    setupPush() {
        this.oneSignal.startInit('9f01cc82-7609-4e4b-950c-d328e7f8592b', '1052570413016');

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);

        this.oneSignal.handleNotificationOpened().subscribe((data: any) => {
            console.log('handleNotificationOpened');
            console.log(data);
            let additionalData = data.notification.payload.additionalData;
            this.clientApiService.showMsg(additionalData);
        });

        this.oneSignal.handleNotificationReceived().subscribe((data: any) => {
            console.log('handleNotificationReceived');
            console.log(data);
            let msg = data.payload.body;
            let title = data.payload.title;
            let additionalData = data.payload.additionalData;

            this.clientApiService.showMsg(msg);
        });

        this.oneSignal.endInit();
    }



    initTranslate() {
        const enLang = 'en';

        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang(enLang);

        if (this.translate.getBrowserLang() !== undefined) {
            this.translate.use(this.translate.getBrowserLang());
        } else {
            this.translate.use(enLang); // Set your language here
        }

        // this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
        //   this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
        // });
    }
}
