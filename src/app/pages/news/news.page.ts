import {Component, OnInit} from '@angular/core';
import {NavController, ToastController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from 'src/app/services/login/login.service';
import {AdvertService} from "../entities/advert";
import {OpenApiService} from "../../services/open-api/open-api.service";

@Component({
    selector: 'news',
    templateUrl: './news.page.html',
    styleUrls: ['./news.page.scss']
})
export class NewsPage implements OnInit {

    adverts: any;

    constructor(
        public translateService: TranslateService,
        public toastController: ToastController,
        public navController: NavController,
        private openApiService: OpenApiService,
        private advertService: AdvertService,

    ) {
    }

    ngOnInit() {
        this.openApiService.findAllAdverts(null).subscribe((resp) => {
            this.adverts = resp.body;
        });
    }

    getNews() {
    }
}
