import {Component, OnInit} from '@angular/core';
import {AccountService} from "../../services/auth/account.service";
import {ContactInfoModal} from "../entities/order";
import {SmsConfirmModal} from "../entities/modals/sms-confirm.modal";
import {Events, ModalController, NavController} from "@ionic/angular";
import {ClientApiService} from "../../services/client-api/client-api.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-account',
    templateUrl: 'account.page.html',
    styleUrls: ['account.page.scss']
})
export class AccountPage implements OnInit {

    account: any;

    constructor(
        private accountService: AccountService,
        private modalController: ModalController,
        private events: Events,
        private navController: NavController,
        private router: Router,
        private clientApiService: ClientApiService
    ) {
        this.events.subscribe('account:refresh', () => {
            this.setAccount();
        });
    }

    ngOnInit(): void {
        console.log('account init');
        this.setAccount();
    }

    setAccount() {
        console.log('account setting');
        this.accountService.identity().then(account => {
            console.log('account idetitied', account);
            this.account = account;
        }, (err) => {
            console.log(err);
            this.account = null;
        });
    }

    inviteFriends() {
        if(!this.account) {
            this.clientApiService.showMsg('Требуется авторизация');
            return;
        }
        this.navController.navigateRoot('/promocode-activate');
    }

}
