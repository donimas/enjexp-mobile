import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: 'welcome.page.html',
  styleUrls: ['welcome.page.scss']
})
export class WelcomePage implements OnInit {
  constructor(
      public navController: NavController
  ) {}

  ngOnInit() {}

    goToMenu() {
        this.navController.navigateRoot('/tabs');
    }

}
