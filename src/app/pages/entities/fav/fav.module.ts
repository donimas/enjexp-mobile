import {NgModule, Injectable} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {UserRouteAccessService} from '../../../services/auth/user-route-access.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {FavService} from "./fav.service";
import {FavPage} from "./fav";
import {OrderAmountModule} from "../order-amount/order-amount.module";
import {AmountModal} from "../menu";

@Injectable({providedIn: 'root'})
export class FavResolve implements Resolve<any> {
    constructor(private service: FavService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<any>) => response.ok),
                map((orderItem: HttpResponse<any>) => orderItem.body)
            );
        }
        return of({});
    }
}

const routes: Routes = [
    {
        path: '',
        component: FavPage,
        data: {
            /*authorities: ['ROLE_USER']*/
        },
        /*canActivate: [UserRouteAccessService]*/
    }, {
        path: 'fav-menu',
        loadChildren: '../menu/menu.module#MenuPageModule'
    }
];

@NgModule({
    declarations: [
        FavPage,
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        OrderAmountModule,
        RouterModule.forChild(routes)
    ],
    exports: [
    ],
    entryComponents: [
        AmountModal
    ]
})
export class FavPageModule {
}