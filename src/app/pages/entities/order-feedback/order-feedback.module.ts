import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { OrderFeedbackPage } from './order-feedback';
import { OrderFeedbackUpdatePage } from './order-feedback-update';
import { OrderFeedback, OrderFeedbackService, OrderFeedbackDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class OrderFeedbackResolve implements Resolve<OrderFeedback> {
  constructor(private service: OrderFeedbackService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderFeedback> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<OrderFeedback>) => response.ok),
        map((orderFeedback: HttpResponse<OrderFeedback>) => orderFeedback.body)
      );
    }
    return of(new OrderFeedback());
  }
}

const routes: Routes = [
    {
      path: '',
      component: OrderFeedbackPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: OrderFeedbackUpdatePage,
      resolve: {
        data: OrderFeedbackResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: OrderFeedbackDetailPage,
      resolve: {
        data: OrderFeedbackResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: OrderFeedbackUpdatePage,
      resolve: {
        data: OrderFeedbackResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        OrderFeedbackPage,
        OrderFeedbackUpdatePage,
        OrderFeedbackDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class OrderFeedbackPageModule {
}
