import { BaseEntity } from 'src/model/base-entity';

export const enum NoticeReceiverType {
    'ONE_USER',
    'GROUP',
    'ALL_USERS',
    'ORDER',
    'STOCK'
}

export const enum NoticeType {
    'INFO',
    'SUCCESS',
    'WARNING',
    'DANGER'
}

export class Notice implements BaseEntity {
    constructor(
        public id?: number,
        public title?: string,
        public content?: string,
        public smsContent?: string,
        public flagPush?: boolean,
        public flagSms?: boolean,
        public flagEmail?: boolean,
        public receiverType?: NoticeReceiverType,
        public createDate?: any,
        public modifyDate?: any,
        public containerClass?: string,
        public containerId?: number,
        public noticeType?: NoticeType,
        public flagSent?: boolean,
        public sentTime?: any,
        public payloadObjectUrl?: string,
        public payloadObjectId?: number,
        public flagDeleted?: boolean,
    ) {
        this.flagPush = false;
        this.flagSms = false;
        this.flagEmail = false;
        this.flagSent = false;
        this.flagDeleted = false;
    }
}
