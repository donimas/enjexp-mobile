import {Component, OnInit} from '@angular/core';
import {NavController, ToastController, Platform, IonItemSliding, ModalController, Events} from '@ionic/angular';
import {filter, map} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';
import {FavService} from "./fav.service";
import {AmountModal} from "../menu";
import {LocalStorageService} from "ngx-webstorage";
import {OpenApiService} from "../../../services/open-api/open-api.service";
import {UserService} from "../../../services/user/user.service";

@Component({
    selector: 'fav-item',
    templateUrl: 'fav.html',
    styleUrls: ['../menu/menu.page.scss']
})
export class FavPage {
    orderItems: any[];
    demo: Array<any> = [
        {name: 'Феттучини "Альфредо"', ingr: 'Куриное филе, сливки, пармезан, грибы, черри', price: '1500', time: '15'},
        {name: 'Карбонара', ingr: 'Говядина, лук, яйцо, сливки, пармезан', price: '1500', time: '15'},
        {
            name: 'Orange Chicken',
            ingr: 'Куриное филе, рисовый уксус, соя, имбирь, апельсин, чеснок, чили, кисло сладкий соус',
            price: '1300',
            time: '15'
        },
        {name: 'Рамен"', ingr: 'Лапша, куриное филе, шампиньоны, яйцо, кочулян, лук, черри', price: '900', time: '15'},
    ];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private favService: FavService,
        private toastCtrl: ToastController,
        private modalController: ModalController,
        private $localStorage: LocalStorageService,
        private openApiService: OpenApiService,
        private userService: UserService,
        private events: Events,
        public plt: Platform
    ) {
        this.orderItems = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        const favs: number[] = this.$localStorage.retrieve('favourites');
        //const favs2: number[] = this.$localStorage.retrieve('favourites');
        console.log(this.$localStorage.getStrategyName());
        console.log(favs);
        if (!favs) {
            return;
        }
        this.openApiService.queryByIds(favs).subscribe((resp: any) => {
                if (resp && resp.body) {
                    this.orderItems = resp.body;
                    console.log(resp);
                }
                if (typeof (refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({
                    message: 'Failed to load data',
                    duration: 2000,
                    position: 'middle'
                });
                toast.present();
            });
    }

    trackId(index: number, item: any) {
        return item.id;
    }

    view(orderItem?: any) {
        //this.navController.navigateForward('/tabs/entities/order-item/' + orderItem.id + '/view');
        this.navController.navigateForward('/tabs/fav/fav-menu/' + orderItem.id + '/view');
    }

    async showAmountModal() {
        const modal = await this.modalController.create({
            component: AmountModal,
            cssClass: 'amount-modal-css',
            showBackdrop: true
        });
        return await modal.present();
    }

    async selectAmount(e) {
        if(!e) {
            return;
        }
        console.log('order item:', e);
        this.userService.addCheckout(e);
        this.events.publish('checkout:amount', e);
    }
}
