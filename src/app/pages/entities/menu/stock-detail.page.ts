import {Component, Input} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'stock-detail',
    templateUrl: 'stock-detail.page.html',
    styleUrls: ['../../news/news.page.scss']
})
export class StockDetailPage {

    @Input() advert: any;

    constructor(
        private modalController: ModalController
    ) {

    }

    ionViewDidEnter() {
        console.log('did load', this.advert);
    }

    close() {
        this.modalController.dismiss();
    }

}