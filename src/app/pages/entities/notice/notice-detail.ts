import { Component, OnInit } from '@angular/core';
import { Notice } from './notice.model';
import { NoticeService } from './notice.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-notice-detail',
    templateUrl: 'notice-detail.html'
})
export class NoticeDetailPage implements OnInit {
    notice: Notice = {};

    constructor(
        private navController: NavController,
        private noticeService: NoticeService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.notice = response.data;
        });
    }

    open(item: Notice) {
        this.navController.navigateForward('/tabs/entities/notice/' + item.id + '/edit');
    }

    async deleteModal(item: Notice) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.noticeService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/notice');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
