import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Notice } from './notice.model';
import { NoticeService } from './notice.service';

@Component({
    selector: 'page-notice',
    templateUrl: 'notice.html'
})
export class NoticePage {
    notices: Notice[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private noticeService: NoticeService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.notices = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.noticeService.query().pipe(
            filter((res: HttpResponse<Notice[]>) => res.ok),
            map((res: HttpResponse<Notice[]>) => res.body)
        )
        .subscribe(
            (response: Notice[]) => {
                this.notices = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Notice) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/notice/new');
    }

    edit(item: IonItemSliding, notice: Notice) {
        this.navController.navigateForward('/tabs/entities/notice/' + notice.id + '/edit');
        item.close();
    }

    async delete(notice) {
        this.noticeService.delete(notice.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'Notice deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(notice: Notice) {
        this.navController.navigateForward('/tabs/entities/notice/' + notice.id + '/view');
    }
}
