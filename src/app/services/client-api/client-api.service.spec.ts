import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ClientApiService} from "./client-api.service";

describe('ClientApiService', () => {
    beforeEach(() =>
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        })
    );

    it('should be created', () => {
        const service: ClientApiService = TestBed.get(ClientApiService);
        expect(service).toBeTruthy();
    });
});
