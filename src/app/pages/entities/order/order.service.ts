import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Order } from './order.model';

@Injectable({ providedIn: 'root'})
export class OrderService {
    private resourceUrl = ApiService.API_URL + '/orders';

    constructor(protected http: HttpClient) { }

    create(order: Order): Observable<HttpResponse<Order>> {
        return this.http.post<Order>(this.resourceUrl, order, { observe: 'response'});
    }

    update(order: Order): Observable<HttpResponse<Order>> {
        return this.http.put(this.resourceUrl, order, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<Order>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<Order[]>> {
        const options = createRequestOption(req);
        return this.http.get<Order[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    getPromocode(code: any): Observable<HttpResponse<any>> {
        return this.http.get(`${ApiService.API_URL}/promocode/check/${code}`, { observe: 'response'});
        ///return this.apiService.get(`${this.resourceUrl}/menus`, null, { params: options, observe: 'response' });
    }

    checkPromocodeOwner(): Observable<HttpResponse<any>> {
        return this.http.get(`${ApiService.API_URL}/promocode/filled`, { observe: 'response'});
        ///return this.apiService.get(`${this.resourceUrl}/menus`, null, { params: options, observe: 'response' });
    }

    clientCheckout(req: any): Observable<HttpResponse<any>> {
        return this.http.post<any>(`${ApiService.API_URL}/client/checkout`, req, { observe: 'response'});
    }

    clientOrders(): Observable<HttpResponse<any>> {
        return this.http.get<any>(`${ApiService.API_URL}/client/orders`, { observe: 'response'});
    }

    clientFindOne(id: number): Observable<HttpResponse<any>> {
        return this.http.get(`${ApiService.API_URL}/client/order/${id}`, { observe: 'response'});
    }

    clientCancelOrder(id: number): Observable<HttpResponse<any>> {
        return this.http.get(`${ApiService.API_URL}/client/cancel/order/${id}`, { observe: 'response'});
    }

    calculate(items: any[]) {
        console.log(items);
        if(!items || !items.length) {
            return;
        }
        let result = 0;
        items.forEach((i) => {
            result += i.amount * i.menu.price.price;
        });
        return result;
    }
}
