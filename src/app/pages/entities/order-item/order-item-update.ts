import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { OrderItem } from './order-item.model';
import { OrderItemService } from './order-item.service';
import { Order, OrderService } from '../order';
import { Menu, MenuService } from '../menu';

@Component({
    selector: 'page-order-item-update',
    templateUrl: 'order-item-update.html'
})
export class OrderItemUpdatePage implements OnInit {

    orderItem: OrderItem;
    orders: Order[];
    menus: Menu[];
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        amount: [null, []],
        type: [null, []],
        order: [null, []],
        menu: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private orderService: OrderService,
        private menuService: MenuService,
        private orderItemService: OrderItemService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.orderService.query()
            .subscribe(data => { this.orders = data.body; }, (error) => this.onError(error));
        this.menuService.query()
            .subscribe(data => { this.menus = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.orderItem = response.data;
            this.isNew = this.orderItem.id === null || this.orderItem.id === undefined;
        });
    }

    updateForm(orderItem: OrderItem) {
        this.form.patchValue({
            id: orderItem.id,
            amount: orderItem.amount,
            type: orderItem.type,
            order: orderItem.order,
            menu: orderItem.menu,
        });
    }

    save() {
        this.isSaving = true;
        const orderItem = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.orderItemService.update(orderItem));
        } else {
            this.subscribeToSaveResponse(this.orderItemService.create(orderItem));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<OrderItem>>) {
        result.subscribe((res: HttpResponse<OrderItem>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `OrderItem ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/order-item');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): OrderItem {
        return {
            ...new OrderItem(),
            id: this.form.get(['id']).value,
            amount: this.form.get(['amount']).value,
            type: this.form.get(['type']).value,
            order: this.form.get(['order']).value,
            menu: this.form.get(['menu']).value,
        };
    }

    compareOrder(first: Order, second: Order): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackOrderById(index: number, item: Order) {
        return item.id;
    }
    compareMenu(first: Menu, second: Menu): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackMenuById(index: number, item: Menu) {
        return item.id;
    }
}
