import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from "@angular/core";
import {FavPage} from "../fav";
import {IonicModule} from "@ionic/angular";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {TranslateModule} from "@ngx-translate/core";
import {OrderAmount} from "./order-amount";
import {AmountModalModule} from "../menu/amount-modal.module";

@NgModule({
    declarations: [
        OrderAmount
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        AmountModalModule,
        TranslateModule
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    exports: [
        OrderAmount
    ]
})
export class OrderAmountModule {
}