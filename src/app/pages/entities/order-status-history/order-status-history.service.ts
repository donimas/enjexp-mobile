import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { OrderStatusHistory } from './order-status-history.model';

@Injectable({ providedIn: 'root'})
export class OrderStatusHistoryService {
    private resourceUrl = ApiService.API_URL + '/order-status-histories';

    constructor(protected http: HttpClient) { }

    create(orderStatusHistory: OrderStatusHistory): Observable<HttpResponse<OrderStatusHistory>> {
        return this.http.post<OrderStatusHistory>(this.resourceUrl, orderStatusHistory, { observe: 'response'});
    }

    update(orderStatusHistory: OrderStatusHistory): Observable<HttpResponse<OrderStatusHistory>> {
        return this.http.put(this.resourceUrl, orderStatusHistory, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<OrderStatusHistory>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<OrderStatusHistory[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderStatusHistory[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
