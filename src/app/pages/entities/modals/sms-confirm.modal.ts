import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {IonInput, ModalController, Platform, ToastController} from "@ionic/angular";
import {OpenApiService} from "../../../services/open-api/open-api.service";
import {LoginService} from "../../../services/login/login.service";
import {TranslateService} from "@ngx-translate/core";
import {UserService} from "../../../services/user/user.service";
import {conformToMask} from "text-mask-core/dist/textMaskCore";

@Component({
    selector: 'sms-confirm-modal',
    templateUrl: 'sms-confirm.modal.html',
    styleUrls: ['sms-confirm.modal.scss']
})
export class SmsConfirmModal implements AfterViewInit, OnInit {

    @ViewChild('val1', {static: false}) val1Input: IonInput;
    @ViewChild('val2', {static: false}) val2Input: IonInput;
    @ViewChild('val3', {static: false}) val3Input: IonInput;
    @ViewChild('val4', {static: false}) val4Input: IonInput;
    isReadyToSave: boolean;
    receivedCode: any = {
        val1: '',
        val2: '',
        val3: '',
        val4: ''
    };
    account: { username: string; password: string; rememberMe: boolean } = {
        username: '',
        password: '',
        rememberMe: true
    };

    @Input() login: string;
    private loginErrorString: string;
    maskedPhone: any;

    flagResend = false;
    resendTime: number = 59;
    isSending = false;
    private phoneMask = ['+', /\d/, ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

    interval: any;

    constructor(
        public platform: Platform,
        private modalController: ModalController,
        private loginService: LoginService,
        private toastController: ToastController,
        public translateService: TranslateService,
        private userService: UserService,
        private openApiService: OpenApiService
    ) {

    }

    ngOnInit(): void {
        if(this.login){
            const masked = conformToMask(this.login, ['+', /\d/, ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/], {guide: false});
            this.maskedPhone = masked.conformedValue;
            console.log('masked phone',this.maskedPhone);
        }
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.val1Input.setFocus();
        }, 100);

        this.startTimer();

        this.translateService.get('LOGIN_ERROR').subscribe(value => {
            this.loginErrorString = value;
        });
    }

    startTimer() {
        this.flagResend = false;
        this.resendTime = 59;
        this.interval = setInterval(() => {
            this.resendTime--;
            if(!this.resendTime) {
                this.flagResend = true;
                console.log('timer is stopped');
                clearInterval(this.interval);
            }
            console.log('timer is working');
        }, 1000);
    }

    resendSmsCode() {
        this.isSending = true;
        let pass = '';
        for(let i=0; i<6; i++) {
            pass = pass + Math.floor(Math.random() * 5)+'';
        }
        const account = {
            login: this.login,
            password: pass
        };
        this.userService.signup(account).subscribe((resp) => {
            this.isSending = false;
            this.startTimer();
        }, (err) => {
            this.isSending = false;
            console.log(err);
        });

        console.log('account created', account);
    }

    cancel() {
        clearInterval(this.interval);
        this.modalController.dismiss();
    }

    activate() {
        console.log(this.receivedCode);
        const code = `${this.receivedCode.val1}${this.receivedCode.val2}${this.receivedCode.val3}${this.receivedCode.val4}`;
        console.log('concatted code', code);
        this.account.username = this.login;
        this.account.password = code;

        this.loginService.login(this.account).then(
            (jwt) => {
                //this.navController.navigateRoot('/tabs');
                console.log('authorized successfully:', jwt);
                clearInterval(this.interval);
                this.modalController.dismiss(jwt);
            },
            async err => {
                // Unable to log in
                this.account.password = '';
                const toast = await this.toastController.create({
                    message: this.loginErrorString,
                    duration: 3000,
                    position: 'top'
                });
                toast.present();
            }
        );
    }

    inputChange(e, alias) {
        console.log(`${alias} pressed`, e);
        let val = e.detail.data;
        if(!val || val.trim().length === 0) {
            console.log('first');
            switch (alias) {
                case 4: this.val3Input.setFocus(); break;
                case 3: this.val2Input.setFocus(); break;
                case 2: this.val1Input.setFocus(); break;
                case 1: break;
            }
        } else if(val.trim().length === 1) {
            console.log('second');
            switch (alias) {
                case 1: this.receivedCode.val1 = val; this.val2Input.setFocus(); break;
                case 2: this.receivedCode.val2 = val; this.val3Input.setFocus(); break;
                case 3: this.receivedCode.val3 = val; this.val4Input.setFocus(); break;
                case 4: this.receivedCode.val4 = val; this.activateByPressingLast(); break;
            }
        } else if(val.trim().length === 4) {
            console.log('third');
            this.receivedCode.val1 = val.charAt(0);
            this.receivedCode.val2 = val.charAt(1);
            this.receivedCode.val3 = val.charAt(2);
            this.receivedCode.val4 = val.charAt(3);
            this.activate();
        }

    }

    initCell() {

    }

    activateByPressingLast() {
        setTimeout(() => {
            this.activate();
        });
    }

}