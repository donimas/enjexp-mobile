import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';

@Injectable({ providedIn: 'root'})
export class FavService {
    private resourceUrl = ApiService.API_URL + '/fav';

    constructor(protected http: HttpClient) { }

    create(fav: any): Observable<HttpResponse<any>> {
        return this.http.post<any>(this.resourceUrl, fav, { observe: 'response'});
    }

    update(fav: any): Observable<HttpResponse<any>> {
        return this.http.put(this.resourceUrl, fav, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<any>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<any[]>> {
        const options = createRequestOption(req);
        return this.http.get<any[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
