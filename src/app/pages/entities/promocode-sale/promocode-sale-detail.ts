import { Component, OnInit } from '@angular/core';
import { PromocodeSale } from './promocode-sale.model';
import { PromocodeSaleService } from './promocode-sale.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-promocode-sale-detail',
    templateUrl: 'promocode-sale-detail.html'
})
export class PromocodeSaleDetailPage implements OnInit {
    promocodeSale: PromocodeSale = {};

    constructor(
        private navController: NavController,
        private promocodeSaleService: PromocodeSaleService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.promocodeSale = response.data;
        });
    }

    open(item: PromocodeSale) {
        this.navController.navigateForward('/tabs/entities/promocode-sale/' + item.id + '/edit');
    }

    async deleteModal(item: PromocodeSale) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.promocodeSaleService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/promocode-sale');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
