import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { RatingTag } from './rating-tag.model';
import { RatingTagService } from './rating-tag.service';

@Component({
    selector: 'page-rating-tag',
    templateUrl: 'rating-tag.html'
})
export class RatingTagPage {
    ratingTags: RatingTag[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private ratingTagService: RatingTagService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.ratingTags = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.ratingTagService.query().pipe(
            filter((res: HttpResponse<RatingTag[]>) => res.ok),
            map((res: HttpResponse<RatingTag[]>) => res.body)
        )
        .subscribe(
            (response: RatingTag[]) => {
                this.ratingTags = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: RatingTag) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/rating-tag/new');
    }

    edit(item: IonItemSliding, ratingTag: RatingTag) {
        this.navController.navigateForward('/tabs/entities/rating-tag/' + ratingTag.id + '/edit');
        item.close();
    }

    async delete(ratingTag) {
        this.ratingTagService.delete(ratingTag.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'RatingTag deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(ratingTag: RatingTag) {
        this.navController.navigateForward('/tabs/entities/rating-tag/' + ratingTag.id + '/view');
    }
}
