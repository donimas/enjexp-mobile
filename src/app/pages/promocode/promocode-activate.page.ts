import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {AccountService} from "../../services/auth/account.service";
import {ClientApiService} from "../../services/client-api/client-api.service";
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import {ApiService} from "../../services/api/api.service";

@Component({
    selector: 'promocode-activate',
    templateUrl: './promocode-activate.page.html',
    styleUrls: ['./promocode-activate.page.scss']
})
export class PromocodeActivatePage implements OnInit {

    // Our translated text strings
    private loginErrorString: string;
    private promocode: any;
    private message: string = '' +
        'Бесплатное блюдо в Enjoy Express до 500 тг: Свежее и горячее Паназиятская и Европейская кухня. Скачай приложение Enjoy Express App и воспользуйся моим пригласительным кодом';
    private urlText: string = '' +
        'Активируй по ссылке';

    constructor(
        private accountService: AccountService,
        private clientApiService: ClientApiService,
        private socialSharing: SocialSharing,
        private apiService: ApiService,
        public navController: NavController
    ) {
    }

    ngOnInit() {

        if(this.checkIsNotAuthenticated()) {
            console.log('user is not authenticated');
            return;
        }

        this.generatePromoCode();
    }

    checkIsNotAuthenticated() {
        if(!this.accountService.isAuthenticated()) {
            this.clientApiService.showMsg('Требуется авторизоваться');
            this.navController.back({animated: true});

            return true;
        }
        return false;
    }

    generatePromoCode() {
        this.apiService.get('create/promocode', null, { observe: 'response'}).subscribe((resp: any) => {
            console.log('promocode generated', resp);
            this.promocode = resp.body.code;
        }, (err) => {
            console.log(err);
        });
    }

    share() {
        this.socialSharing.share(`${this.message} ${this.promocode}. ${this.urlText}`, 'Enjoy Express App', 'file', 'http://sapasoft.kz');
    }
    shareViaWhatsapp() {
        this.socialSharing.shareViaWhatsApp(`${this.message} ${this.promocode}. ${this.urlText}`, null, 'http://sapasoft.kz');
    }
    shareViaMessage() {
        this.socialSharing.shareViaSMS(`${this.message} ${this.promocode}. ${this.urlText} http://sapasoft.kz`, null);
    }
}
