import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {PromocodeActivatePage} from "./promocode-activate.page";

const routes: Routes = [
    {
        path: '',
        component: PromocodeActivatePage
    }
];

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes), TranslateModule],
    declarations: [PromocodeActivatePage]
})
export class PromocodeActivatePageModule {
}
