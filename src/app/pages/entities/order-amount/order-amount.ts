import {Component, EventEmitter, Input, Output} from "@angular/core";
import {AmountModal} from "../menu";
import {ModalController} from "@ionic/angular";

@Component({
    selector: 'order-amount',
    templateUrl: 'order-amount.html',
    styleUrls: ['order-amount.scss']
})
export class OrderAmount {

    @Input('title') title: any;
    @Input('size') size: any;
    @Input('color') color: any;
    @Input('classes') classes: any[];
    @Input('menu') menu: any;
    @Input('icon') icon: any;
    @Input('expand') expand: any;
    @Input('img') img: any;

    @Output() valueChanged = new EventEmitter<string>();


    constructor(
        private modalController: ModalController,
    ) {
    }

    async showAmountModal() {
        console.log('component modal');
        const modal = await this.modalController.create({
            component: AmountModal,
            cssClass: 'amount-modal-css',
            componentProps: {
                menu: this.menu,
            },
            showBackdrop: true
        });
        modal.onDidDismiss().then((data) => {
            const result = data['data']; // Here's your selected user!
            console.log('on dismiss data', result);
            this.valueChanged.emit(result);
        });
        return await modal.present();
    }

}