import { BaseEntity } from 'src/model/base-entity';

export class Advert implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public imageContentType?: string,
        public image?: any,
    ) {
    }
}
