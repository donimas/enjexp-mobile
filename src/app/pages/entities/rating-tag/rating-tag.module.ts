import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { RatingTagPage } from './rating-tag';
import { RatingTagUpdatePage } from './rating-tag-update';
import { RatingTag, RatingTagService, RatingTagDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class RatingTagResolve implements Resolve<RatingTag> {
  constructor(private service: RatingTagService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<RatingTag> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<RatingTag>) => response.ok),
        map((ratingTag: HttpResponse<RatingTag>) => ratingTag.body)
      );
    }
    return of(new RatingTag());
  }
}

const routes: Routes = [
    {
      path: '',
      component: RatingTagPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: RatingTagUpdatePage,
      resolve: {
        data: RatingTagResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: RatingTagDetailPage,
      resolve: {
        data: RatingTagResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: RatingTagUpdatePage,
      resolve: {
        data: RatingTagResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        RatingTagPage,
        RatingTagUpdatePage,
        RatingTagDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class RatingTagPageModule {
}
