import {NgModule, Injectable} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {IonicModule} from '@ionic/angular';
import {Camera} from '@ionic-native/camera/ngx';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {UserRouteAccessService} from '../../../services/auth/user-route-access.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';

import {MenuPage} from './menu';
import {MenuUpdatePage} from './menu-update';
import {AmountModal} from './amount.modal';
import {Menu, MenuService, MenuDetailPage} from '.';
import {StarRating} from 'ionic4-star-rating';
import {OrderAmount} from "../order-amount/order-amount";
import {OrderAmountModule} from "../order-amount/order-amount.module";
import {AmountModalModule} from "./amount-modal.module";
import {OpenApiService} from "../../../services/open-api/open-api.service";

@Injectable({providedIn: 'root'})
export class MenuResolve implements Resolve<Menu> {
    constructor(
        private service: MenuService,
        private openApiService: OpenApiService,
        ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Menu> {
        const id = route.params.id ? route.params.id : null;
        if (id) {
            //return this.service.find(id).pipe(
            return this.openApiService.findMenu(id).pipe(
                filter((response: HttpResponse<Menu>) => response.ok),
                map((menu: HttpResponse<Menu>) => menu.body)
            );
        }
        return of(new Menu());
    }
}

const routes: Routes = [
    {
        path: '',
        component: MenuPage,
        data: {
            /*authorities: []*/
        }/*,
        canActivate: [UserRouteAccessService]*/
    },
    {
        path: 'new',
        component: MenuUpdatePage,
        resolve: {
            data: MenuResolve
        },
        data: {
            authorities: ['ROLE_USER']
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: MenuDetailPage,
        resolve: {
            data: MenuResolve
        },
        data: {
            /*authorities: ['ROLE_USER']*/
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: MenuUpdatePage,
        resolve: {
            data: MenuResolve
        },
        data: {
            authorities: ['ROLE_USER']
        },
        canActivate: [UserRouteAccessService]
    }
];


@NgModule({
    declarations: [
        MenuPage,
        MenuUpdatePage,
        MenuDetailPage,
        StarRating,
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        OrderAmountModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        StarRating,
    ],
    providers: [Camera],
    entryComponents: [
        AmountModal
    ],
})
export class MenuPageModule {
}