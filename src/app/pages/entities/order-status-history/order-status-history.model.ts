import { BaseEntity } from 'src/model/base-entity';

export const enum OrderStatus {
    'NEW',
    'CONFIRMED',    //warning
    'IN_PROCESS',   //primary
    'FINISHED',     //success
    'DELIVERING',   //secondary
    'DELIVERED',    //success
    'CANCELED',     //danger
    'REJECTED'      //tertiary
}

export class OrderStatusHistory implements BaseEntity {
    constructor(
        public id?: number,
        public orderStatus?: OrderStatus,
        public createDate?: any,
        public comment?: string,
        public order?: any,
    ) {
    }
}
