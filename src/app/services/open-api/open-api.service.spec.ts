import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {OpenApiService} from "./open-api.service";

describe('OpenApiService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    })
  );

  it('should be created', () => {
    const service: OpenApiService = TestBed.get(OpenApiService);
    expect(service).toBeTruthy();
  });
});
