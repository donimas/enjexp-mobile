import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { OrderStatusHistory } from './order-status-history.model';
import { OrderStatusHistoryService } from './order-status-history.service';
import { Order, OrderService } from '../order';

@Component({
    selector: 'page-order-status-history-update',
    templateUrl: 'order-status-history-update.html'
})
export class OrderStatusHistoryUpdatePage implements OnInit {

    orderStatusHistory: OrderStatusHistory;
    orders: Order[];
    createDate: string;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        orderStatus: [null, []],
        createDate: [null, []],
        comment: [null, []],
        order: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private orderService: OrderService,
        private orderStatusHistoryService: OrderStatusHistoryService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.orderService.query()
            .subscribe(data => { this.orders = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.orderStatusHistory = response.data;
            this.isNew = this.orderStatusHistory.id === null || this.orderStatusHistory.id === undefined;
        });
    }

    updateForm(orderStatusHistory: OrderStatusHistory) {
        this.form.patchValue({
            id: orderStatusHistory.id,
            orderStatus: orderStatusHistory.orderStatus,
            createDate: (this.isNew) ? new Date().toISOString() : orderStatusHistory.createDate,
            comment: orderStatusHistory.comment,
            order: orderStatusHistory.order,
        });
    }

    save() {
        this.isSaving = true;
        const orderStatusHistory = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.orderStatusHistoryService.update(orderStatusHistory));
        } else {
            this.subscribeToSaveResponse(this.orderStatusHistoryService.create(orderStatusHistory));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<OrderStatusHistory>>) {
        result.subscribe((res: HttpResponse<OrderStatusHistory>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `OrderStatusHistory ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/order-status-history');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): OrderStatusHistory {
        return {
            ...new OrderStatusHistory(),
            id: this.form.get(['id']).value,
            orderStatus: this.form.get(['orderStatus']).value,
            createDate: new Date(this.form.get(['createDate']).value),
            comment: this.form.get(['comment']).value,
            order: this.form.get(['order']).value,
        };
    }

    compareOrder(first: Order, second: Order): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackOrderById(index: number, item: Order) {
        return item.id;
    }
}
