import {Component, Input, ViewChild} from '@angular/core';
import {Events, IonInput, ModalController, NavController, NavParams, Platform, ToastController} from '@ionic/angular';
import {FormBuilder, FormControl} from "@angular/forms";
import {UserService} from "../../../services/user/user.service";
import {TranslateService} from "@ngx-translate/core";
import {createTextMaskInputElement} from "text-mask-core/dist/textMaskCore";
import {SmsConfirmModal} from "../modals/sms-confirm.modal";

@Component({
    selector: 'contact-info-modal',
    templateUrl: 'contact-info.modal.html',
    styleUrls: ['contact-info.modal.scss']
})
export class ContactInfoModal {

    isReadyToSave: boolean;
    contactInfo: any;
    isSaving = false;
    isNew = true;
    account: {
        login: string;
        firstName: string;
        langKey: string;
        password: string;
    } = {
        login: '',
        firstName: '',
        langKey: 'ru',
        password: ''
    };
    private signupErrorString: string;
    private signupSuccessString: string;
    private existingUserError: string;
    private invalidPasswordError: string;

    @ViewChild('phoneInput', {static: false})
    public set phoneInput(value: IonInput) {
        if (!value) {
            return;
        }

        value.getInputElement().then(input => this.registerTextMask(input));
    }
    private phoneMask = ['+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

    form = this.formBuilder.group({
        id: [],
        fullName: [null, []],
        phone: [null, []],
    });

    constructor(
        protected formBuilder: FormBuilder,
        protected toastCtrl: ToastController,
        protected navController: NavController,
        private userService: UserService,
        private translateService: TranslateService,
        public platform: Platform,
        private events: Events,
        public modalController: ModalController,
        ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });
        this.translateService.get(['SIGNUP_ERROR', 'SIGNUP_SUCCESS', 'EXISTING_USER_ERROR', 'INVALID_PASSWORD_ERROR']).subscribe(values => {
            this.signupErrorString = values.SIGNUP_ERROR;
            this.signupSuccessString = values.SIGNUP_SUCCESS;
            this.existingUserError = values.EXISTING_USER_ERROR;
            this.invalidPasswordError = values.INVALID_PASSWORD_ERROR;
        });
    }

    private registerTextMask(inputElement: HTMLInputElement) {
        const maskedInput = createTextMaskInputElement({
            inputElement,
            mask: this.phoneMask,
            guide: true,
            showMask: true
        });
        this.phoneNumber.valueChanges.subscribe(value => {
            maskedInput.update(value);
        });
    }

    get phoneNumber() {
        return this.form.get('phone') as FormControl;
    }

    ionViewDidEnter() {
        console.log('did load');
    }

    doSignup(val?:string) {
        console.log('doing sign up', val);
        this.isSaving = true;
        // set login to same as email
        this.account = this.createFromForm();
        let login = '';
        const temp = this.account.login;
        for(let i=0; i<temp.length; i++) {
            if(temp.charAt(i).match(/\d/)) {
                login = login + temp.charAt(i);
            }
        }
        this.account.login = login;
        console.log('account created', this.account);
        // Attempt to login in through our User service
        this.userService.signup(this.account).subscribe(
            async () => {
                this.account.password = null;
                //this.modalController.dismiss(this.account);
                this.showSmsConfirmModal(this.account);
            },
            async response => {
                console.log(response);
                if(response.statusText === 'Unknown Error') {
                    this.account.password = null;
                    //this.modalController.dismiss(this.account);
                    this.showSmsConfirmModal(this.account);
                    return;
                }
                // Unable to sign up
                const error = JSON.parse(response.error);
                let displayError = this.signupErrorString;
                if (response.status === 400 && error.type.includes('already-used')) {
                    displayError = this.existingUserError;
                } else if (
                    response.status === 400 &&
                    error.message === 'error.validation' &&
                    error.fieldErrors[0].field === 'password' &&
                    error.fieldErrors[0].message === 'Size'
                ) {
                    displayError = this.invalidPasswordError;
                }
                const toast = await this.toastCtrl.create({
                    message: displayError,
                    duration: 3000,
                    position: 'middle'
                });
                toast.present();
            }
        );
    }

    async showSmsConfirmModal(account: any) {
        const modal = await this.modalController.create({
            component: SmsConfirmModal,
            showBackdrop: true,
            componentProps: {
                'login': account.login
            }
        });
        modal.onDidDismiss().then((data) => {
            console.log(data);
            if(!data) {
                console.log('data is empty');
                return;
            }
            this.events.publish('account:refresh');
            this.navController.back({animated: true});
            //this.navController.navigateForward('/tabs/account', {animated: true});
        });
        return await modal.present();
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): any {
        this.account.login = this.form.get(['phone']).value;
        this.account.firstName = this.form.get(['fullName']).value;

        for(let i=0; i<6; i++) {
            this.account.password = this.account.password + Math.floor(Math.random() * 5)+'';
        }

        console.log('account created', this.account);
        return this.account;
    }

}