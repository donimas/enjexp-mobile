import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {UserRouteAccessService} from 'src/app/services/auth/user-route-access.service';
import {EntitiesPage} from './entities.page';

const routes: Routes = [
    {
        path: '',
        component: EntitiesPage,
        data: {
            authorities: ['ROLE_USER']
        },
        canActivate: [UserRouteAccessService]
    }
    , {
        path: 'menu',
        loadChildren: './menu/menu.module#MenuPageModule'
    }
    , {
        path: 'category',
        loadChildren: './category/category.module#CategoryPageModule'
    }
    , {
        path: 'price-history',
        loadChildren: './price-history/price-history.module#PriceHistoryPageModule'
    }
    , {
        path: 'order',
        loadChildren: './order/order.module#OrderPageModule'
    }
    , {
        path: 'location',
        loadChildren: './location/location.module#LocationPageModule'
    }
    , {
        path: 'order-item',
        loadChildren: './order-item/order-item.module#OrderItemPageModule'
    }
    , {
        path: 'order-status-history',
        loadChildren: './order-status-history/order-status-history.module#OrderStatusHistoryPageModule'
    }
    , {
        path: 'order-feedback',
        loadChildren: './order-feedback/order-feedback.module#OrderFeedbackPageModule'
    }
    , {
        path: 'rating-tag',
        loadChildren: './rating-tag/rating-tag.module#RatingTagPageModule'
    }
    , {
        path: 'promocode',
        loadChildren: './promocode/promocode.module#PromocodePageModule'
    }
    , {
        path: 'promocode-sale',
        loadChildren: './promocode-sale/promocode-sale.module#PromocodeSalePageModule'
    }
    , {
        path: 'advert',
        loadChildren: './advert/advert.module#AdvertPageModule'
    }
    , {
        path: 'notice',
        loadChildren: './notice/notice.module#NoticePageModule'
    }
    , {
        path: 'notice-receiver',
        loadChildren: './notice-receiver/notice-receiver.module#NoticeReceiverPageModule'
    }
    , {
        path: 'fav',
        loadChildren: './fav/fav.module#FavPageModule'
    }
    /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
];

@NgModule({
    imports: [IonicModule, CommonModule, FormsModule, RouterModule.forChild(routes), TranslateModule],
    declarations: [EntitiesPage]
})
export class EntitiesPageModule {
}
