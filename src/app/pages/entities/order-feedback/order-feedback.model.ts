import { BaseEntity } from 'src/model/base-entity';
import { Order } from '../order/order.model';
import { RatingTag } from '../rating-tag/rating-tag.model';

export class OrderFeedback implements BaseEntity {
    constructor(
        public id?: number,
        public rating?: number,
        public comment?: string,
        public createDate?: any,
        public order?: Order,
        public ratingTags?: RatingTag[],
    ) {
    }
}
