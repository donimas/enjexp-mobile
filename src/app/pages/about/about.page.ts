import {Component, OnInit} from '@angular/core';
import {NavController, ToastController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from 'src/app/services/login/login.service';

@Component({
    selector: 'about',
    templateUrl: './about.page.html',
    styleUrls: ['../news/news.page.scss']
})
export class AboutPage implements OnInit {

    constructor(
        public translateService: TranslateService,
        public toastController: ToastController,
        public navController: NavController
    ) {
    }

    ngOnInit() {
    }

    getAbout() {
    }
}
