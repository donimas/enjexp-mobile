import { Component, OnInit } from '@angular/core';
import { Promocode } from './promocode.model';
import { PromocodeService } from './promocode.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-promocode-detail',
    templateUrl: 'promocode-detail.html'
})
export class PromocodeDetailPage implements OnInit {
    promocode: Promocode = {};

    constructor(
        private navController: NavController,
        private promocodeService: PromocodeService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.promocode = response.data;
        });
    }

    open(item: Promocode) {
        this.navController.navigateForward('/tabs/entities/promocode/' + item.id + '/edit');
    }

    async deleteModal(item: Promocode) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.promocodeService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/promocode');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
