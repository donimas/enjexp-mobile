import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import {Menu} from "../../pages/entities/menu";
import {Category} from "../../pages/entities/category";
import {ToastController} from "@ionic/angular";

@Injectable({ providedIn: 'root'})
export class ClientApiService {

    constructor(
            private toastController: ToastController
        ) { }

    async showFLCError(err: any) {
        console.log('showFLCError err', err);
        if(!err.error || !err.error.detail) {
            return;
        }

        const toast = await this.toastController.create({
            message: err.error.detail,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    async showMsg(msg: any) {
        console.log('show msg', msg);

        const toast = await this.toastController.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }
}
