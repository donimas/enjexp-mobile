import {Component, OnInit} from '@angular/core';
import {NavController, ToastController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'history',
    templateUrl: './history.page.html',
    styleUrls: ['./history.page.scss']
})
export class HistoryPage implements OnInit {

    constructor(
        public translateService: TranslateService,
        public toastController: ToastController,
        public navController: NavController
    ) {
    }

    ngOnInit() {
    }

    submit() {
    }
}
