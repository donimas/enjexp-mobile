import {Component, Input, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
    selector: 'amount-modal',
    templateUrl: 'amount.modal.html',
    styleUrls: ['amount.modal.scss']
})
export class AmountModal implements OnInit{

    @Input() menu: any;
    amount: number = 1;
    spicyLevel = 'MIDDLE';

    constructor(
        private modalController: ModalController
    ) {

    }

    ngOnInit(): void {
        console.log('did init', this.menu);
        if(this.menu.spicy) {
            const el: HTMLElement = document.querySelector('.amount-modal-css .modal-wrapper');
            el.style.setProperty('max-height', '330px');
        }
    }

    ionViewDidEnter() {
        console.log('did load', this.menu);
        /* const el = document.querySelector('.inner-scroll');
        el.style.setProperty('overflow', 'hidden'); */
    }

    selectAmount() {
        let result = {
            menu: this.menu,
            amount: this.amount,
            spicyLevel: null
        };
        if(this.menu.spicy) {
            result.spicyLevel = this.spicyLevel;
        }
        console.log('amount selected', result);
        this.modalController.dismiss(result);
    }

    addAmount() {
        this.amount++;
    }

    removeAmount() {
        if(this.amount === 1) {
            return;
        }
        this.amount--;
    }

    spicyLevelChange($e) {
        console.log($e);
        this.spicyLevel = $e.detail.value;
    }

}