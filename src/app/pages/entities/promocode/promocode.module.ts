import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { PromocodePage } from './promocode';
import { PromocodeUpdatePage } from './promocode-update';
import { Promocode, PromocodeService, PromocodeDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class PromocodeResolve implements Resolve<Promocode> {
  constructor(private service: PromocodeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Promocode> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Promocode>) => response.ok),
        map((promocode: HttpResponse<Promocode>) => promocode.body)
      );
    }
    return of(new Promocode());
  }
}

const routes: Routes = [
    {
      path: '',
      component: PromocodePage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: PromocodeUpdatePage,
      resolve: {
        data: PromocodeResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: PromocodeDetailPage,
      resolve: {
        data: PromocodeResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: PromocodeUpdatePage,
      resolve: {
        data: PromocodeResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        PromocodePage,
        PromocodeUpdatePage,
        PromocodeDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class PromocodePageModule {
}
