import {Component, OnInit} from '@angular/core';
import {ModalController, NavController} from '@ionic/angular';
import {AccountService} from 'src/app/services/auth/account.service';
import {LoginService} from 'src/app/services/login/login.service';
import {Account} from 'src/model/account.model';
import {ContactInfoModal} from "../entities/order";
import {StockDetailPage} from "../entities/menu/stock-detail.page";
import {AdvertService} from "../entities/advert";
import {OpenApiService} from "../../services/open-api/open-api.service";

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
    account: Account;
    adverts: any;

    slideOptsTwo = {
        initialSlide: 0,
        slidesPerView: 1.1,
        loop: false,
        centeredSlides: false
    };
    entities: Array<any> = [
        {name: 'Menu', component: 'MenuPage', route: 'regular-menu'},
    ];

    constructor(
        public navController: NavController,
        private accountService: AccountService,
        private loginService: LoginService,
        private modalController: ModalController,
        private openApiService: OpenApiService,
        private advertService: AdvertService,
        ) {
    }

    ngOnInit() {
        console.log('home page');
        this.loadAdverts();
        /* this.accountService.identity().then(account => {
          if (account === null) {
            this.goBackToHomePage();
          } else {
            this.account = account;
          }
        }); */
    }

    loadAdverts() {
        this.openApiService.findAllAdverts(null).subscribe((resp) => {
            console.log(resp);
            this.adverts = resp.body;
        }, (err) => {
            console.log(err);
        });
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    logout() {
        this.loginService.logout();
        this.goBackToHomePage();
    }

    private goBackToHomePage(): void {
        this.navController.navigateBack('');
    }

    openPage(page) {
        this.navController.navigateForward('/tabs/home/' + page.route);
    }

    async viewStock(advert) {
        const modal = await this.modalController.create({
            component: StockDetailPage,
            componentProps: {
                'advert': advert,
            }
        });
        return await modal.present();
    }
}
