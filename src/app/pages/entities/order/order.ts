import {Component} from '@angular/core';
import {
    NavController,
    ToastController,
    Platform,
    IonItemSliding,
    PickerController,
    AlertController, Events
} from '@ionic/angular';
import {filter, map} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';
import {Order} from './order.model';
import {OrderService} from './order.service';
import {UserService} from "../../../services/user/user.service";
import {OrderStatus} from "../order-status-history";

@Component({
    selector: 'page-order',
    templateUrl: 'order.html',
    styleUrls: ['order.page.scss']
})
export class OrderPage {
    orders: any;
    checkoutItems: any[];
    pickUpTime: Date;

    subtotal: number;

    // todo: add pagination

    constructor(
        private navController: NavController,
        private orderService: OrderService,
        private toastCtrl: ToastController,
        public plt: Platform,
        private userService: UserService,
        private alertController: AlertController,
        private events: Events,
        public pickerCtrl: PickerController
    ) {
        this.orders = [];
    }

    ionViewWillEnter() {
        console.log('ion view will enter');
        this.loadCheckoutItems();
        this.loadAll();
        this.pickUpTime = null;
    }

    loadCheckoutItems() {
        this.checkoutItems = this.userService.getCheckoutItems();
        console.log('checkout items', this.checkoutItems);
        this.calculateSubtotalSum(this.checkoutItems);
    }

    calculateSubtotalSum(items) {
        this.subtotal = this.orderService.calculate(items);
    }

    ionViewDidEnter() {
        console.log('view did enter');
        const el = document.querySelector('.button-native');
        console.log(el);
        // el.style.setProperty('padding', '0');
    }

    async loadAll(refresher?) {
        this.orderService.clientOrders().pipe(
            filter((res: HttpResponse<any>) => res.ok),
            map((res: HttpResponse<any>) => res.body)
        )
            .subscribe(
                (response: any) => {
                    this.orders = response;
                    console.log('orders', this.orders);
                    if(this.orders && this.orders.length) {
                        this.orders.forEach((o) => {
                            switch(o.statusHistory.orderStatus) {
                                case 'CONFIRMED': o.color = 'warning'; break;
                                case 'IN_PROCESS': o.color = 'primary'; break;
                                case 'FINISHED': o.color = 'success'; break;
                                case 'DELIVERING': o.color = 'secondary'; break;
                                case 'REJECTED': o.color = 'tertiary'; break;
                                default: o.color = 'warning'; break;
                            }
                        });
                    }
                    if (typeof (refresher) !== 'undefined') {
                        setTimeout(() => {
                            refresher.target.complete();
                        }, 750);
                    }
                },
                async (error) => {
                    console.error(error);
                    const toast = await this.toastCtrl.create({
                        message: 'Failed to load data',
                        duration: 2000,
                        position: 'middle'
                    });
                    //toast.present();
                });
    }

    trackId(index: number, item: Order) {
        return item.id;
    }

    proceed() {
        this.navController.navigateForward('/tabs/entities/order/new');
    }

    new() {
        this.navController.navigateForward('/tabs/entities/order/new');
    }

    edit(item: IonItemSliding, order: Order) {
        this.navController.navigateForward('/tabs/entities/order/' + order.id + '/edit');
        item.close();
    }

    async delete(order) {
        this.orderService.delete(order.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'Order deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    async view(order?: Order) {
        //this.navController.navigateForward('/tabs/entities/order/' + order.id + '/view');
        const now = new Date();
        if(this.pickUpTime < now) {
            this.presentPickupDateInvalidAlert();
            return;
        }
        //this.navController.navigateForward('/tabs/checkout/' + 1201 + '/view');
        this.userService.setPickupTime(this.pickUpTime);
        this.navController.navigateForward('/tabs/checkout/preview');
    }

    async viewOrder(order: any) {
        this.navController.navigateForward('/tabs/checkout/' + order.id + '/view');
    }

    async presentPickupDateInvalidAlert() {
        const alert = await this.alertController.create({
            header: 'Некорректное время',
            message: 'У Вас выбрано прошедшее время. Выберите время еще раз, пожалуйста',
            buttons: ['OK']
        });

        await alert.present();
    }

    startMyOrder() {
        //this.navController.navigateForward('/tabs/entities/order/' + order.id + '/view');
        this.navController.navigateForward('/tabs/home/regular-menu');
    }

    generateTimePicker(now: Date) {
        //как можно скорее
        let lastOrderTime = new Date();
        lastOrderTime.setHours(18, 59);
        let days = [];
        if (now <= lastOrderTime) {
            days.push({
                text: 'Сегодня',
                value: now
            });
            // today
        }
        let tomDate = new Date();
        tomDate.setDate(tomDate.getDate() + 1);
        days.push({
            text: 'Завтра',
            value: tomDate
        });

        let daysAmount = 14;
        while(daysAmount > 0) {
            tomDate.setDate(tomDate.getDate() + 1);
            if(tomDate.getDay() != 0) {
                let dayName = this.getDayTitle(tomDate.getDay());
                dayName += ' ' + this.getMonthTitle(tomDate.getMonth() + 1);
                dayName += ' ' + tomDate.getDate();
                days.push({
                    text: dayName,
                    value: tomDate
                });
            }
            daysAmount--;
        }

        console.log('days tom tod gened', days);
        return days;
    }

    getDayTitle(dayNumber) {
        let result = '';
        switch (dayNumber) {
            case 1: result = 'Пн'; break;
            case 2: result = 'Вт'; break;
            case 3: result = 'Ср'; break;
            case 4: result = 'Чт'; break;
            case 5: result = 'Пт'; break;
            case 6: result = 'Сб'; break;
        }
        return result;
    }

    getMonthTitle(monthNumber) {
        let result = '';
        switch (monthNumber) {
            case 1: result = 'Янв'; break;
            case 2: result = 'Фев'; break;
            case 3: result = 'Мар'; break;
            case 4: result = 'Апр'; break;
            case 5: result = 'Май'; break;
            case 6: result = 'Июн'; break;
            case 7: result = 'Июл'; break;
            case 8: result = 'Авг'; break;
            case 9: result = 'Сен'; break;
            case 10: result = 'Окт'; break;
            case 11: result = 'Ноя'; break;
            case 12: result = 'Дек'; break;
        }

        return result;
    }

    generateHours (now: Date) {
        let startHour = 9;
        let result = {
            hours: [],
            selectedIndex: 0
        };
        let i = 0;
        while(startHour < 19) {
            let h = {
                text: startHour,
                value: startHour,
                selected: false
            };
            if(now.getHours() === startHour) {
                result.selectedIndex = i;
                if(now.getMinutes() > 50) {
                    result.selectedIndex++;
                }
            }
            result.hours.push(h);
            startHour++;
            i++;
        }
        if(result.selectedIndex === result.hours.length) {
            result.selectedIndex--;
        }
        return result;
    }

    generateMinutes(now: Date) {
        let minutesAmount = 0;
        let result = {
            minutes: [],
            selectedIndex: 0
        };
        let i = 0;
        while(minutesAmount < 60) {
            result.minutes.push({
                text: minutesAmount < 10 ? '0'+minutesAmount: minutesAmount,
                value: minutesAmount
            });
            if(now.getMinutes() === minutesAmount) {
                result.selectedIndex = i+1;
            } else if(now.getMinutes() > minutesAmount && now.getMinutes() < minutesAmount + 5) {
                result.selectedIndex = i+2;
            }
            /*
            * 00 - 02 - 05  +2
            * 05 - 07 - 10
            * 10 - 11 - 15
            * 15 - 15 - 20
            * 20 - 22 - 25
            * 25 - 29 - 30
            * 30 - 31 - 35
            * 35 - 37 - 40
            * 40 - 44 - 45
            * 45 - 45 - 50
            * 50 - 54 - 55
            * 55 - 57 - 60
            * */

            minutesAmount += 5;
            i++;
        }
        if(result.minutes.length <= result.selectedIndex) {
            if(now.getHours() < 18) {
                result.selectedIndex = result.selectedIndex - result.minutes.length;
            } else if (now.getHours() == 18) {
                result.selectedIndex = result.minutes.length - 1;
            }
        }
        if(now.getHours()<9 || now.getHours() >= 19) {
            result.selectedIndex = 0;
        }
        return result;
    }

    async openPicker() {
        const now = new Date();
        const generatedHours = this.generateHours(now);
        const generatedMinutes = this.generateMinutes(now);
        const picker = await this.pickerCtrl.create({
            columns: [
                {
                    name: 'days',
                    options: this.generateTimePicker(now)
                },
                {
                    name: 'hour',
                    options: generatedHours.hours
                },
                {
                    name: 'minute',
                    options: generatedMinutes.minutes
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Confirm',
                    handler: (value) => {
                        console.log(value);
                        this.pickUpTime = value.days.value;
                        this.pickUpTime.setHours(value.hour.value, value.minute.value);
                    }
                }
            ]
        });
        picker.columns[1].selectedIndex = generatedHours.selectedIndex;
        picker.columns[2].selectedIndex = generatedMinutes.selectedIndex;
        await picker.present();
    }

    addAmount(item) {
        item.amount++;

        this.calculateSubtotalSum(this.checkoutItems);
    }

    removeAmount(item) {
        if (item.amount > 1) {
            item.amount--;
            this.calculateSubtotalSum(this.checkoutItems);
        } else {
            this.removeCheckoutItemAlert(item);
        }
    }

    async removeCheckoutItemAlert(item: any) {
        const alert = await this.alertController.create({
            header: 'Удаление!',
            message: 'Вы действительно хотите убрать?',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Да',
                    handler: () => {
                        console.log('Confirm Okay');
                        this.removeItemFromCheckout(item);
                    }
                }
            ]
        });

        await alert.present();
    }

    removeItemFromCheckout(item: any) {
        const i = this.checkoutItems.indexOf(item);
        this.checkoutItems.splice(i, 1);
        console.log(this.checkoutItems);
        this.calculateSubtotalSum(this.checkoutItems);
        this.userService.setCheckoutItems(this.checkoutItems);
        this.events.publish('checkout:amount', item);
    }
}
