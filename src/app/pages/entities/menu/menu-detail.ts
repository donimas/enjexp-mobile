import { Component, OnInit } from '@angular/core';
import { JhiDataUtils } from 'ng-jhipster';
import { Menu } from './menu.model';
import { MenuService } from './menu.service';
import {NavController, AlertController, Events} from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import {LocalStorageService} from "ngx-webstorage";
import {UserService} from "../../../services/user/user.service";

@Component({
    selector: 'page-menu-detail',
    templateUrl: 'menu-detail.html',
    styleUrls: ['menu-detail.page.scss']
})
export class MenuDetailPage implements OnInit {
    menu: Menu = {};
    selectedSegment = 0;
    flagFav = false;

    constructor(
        private dataUtils: JhiDataUtils,
        private navController: NavController,
        private menuService: MenuService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController,
        private userService: UserService,
        private events: Events,
        private $localStorage: LocalStorageService
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.menu = response.data;
            this.checkFav(this.menu);
        });
    }

    open(item: Menu) {
        this.navController.navigateForward('/tabs/entities/menu/' + item.id + '/edit');
    }

    async deleteModal(item: Menu) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.menuService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/menu');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    logRatingChange(e) {
        console.log('rating changed', e);
    }

    switchSegment(i) {
        this.selectedSegment = i;
    }

    selectAmount(e) {
        console.log('menu detail select amount result', e);
        if(!e) {
            return;
        }
        console.log('order item:', e);
        this.userService.addCheckout(e);
        this.events.publish('checkout:amount', e);
    }

    goBack() {
        this.navController.back();
        /*if(this.navController.canGoBack()) {
            console.log('yes go back');
        } else {
            console.log('no cant go back');
            window.history.back();
        }*/
        //this.navController.navigateBack('/tabs/entities/location');
    }

    addToFavourite(menu) {
        let favs: number[] = this.$localStorage.retrieve('favourites');
        if(favs) {
            const i = favs.indexOf(menu.id);
            if(i === -1) {
                favs.push(menu.id);
            } else {
                favs.splice(i, 1);
            }
        } else {
            favs = [menu.id];
        }
        this.$localStorage.store('favourites', favs);
        this.flagFav = !this.flagFav;
    }

    checkFav(menu) {
        const favs: number[] = this.$localStorage.retrieve('favourites');
        if(!favs) {
            return;
        }
        this.flagFav = favs.indexOf(menu.id) > -1;
    }

}
