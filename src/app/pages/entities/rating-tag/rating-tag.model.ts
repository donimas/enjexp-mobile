import { BaseEntity } from 'src/model/base-entity';

export class RatingTag implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public eq?: number,
        public code?: string,
    ) {
    }
}
