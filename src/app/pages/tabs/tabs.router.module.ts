import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';

const routes: Routes = [
    {
        path: '',
        component: TabsPage,
        children: [
            {
                path: 'home',
                children: [
                    {
                        path: '',
                        loadChildren: '../home/home.module#HomePageModule'
                    }
                ]
            },
            {
                path: 'entities',
                children: [
                    {
                        path: '',
                        loadChildren: '../entities/entities.module#EntitiesPageModule'
                    }
                ]
            },
            {
                path: 'fav',
                children: [
                    {
                        path: '',
                        loadChildren: '../entities/fav/fav.module#FavPageModule'
                    }
                ]
            },
            {
                path: 'checkout',
                children: [
                    {
                        path: '',
                        loadChildren: '../entities/order/order.module#OrderPageModule'
                    }
                ]
            },
            {
                path: 'account',
                children: [
                    {
                        path: '',
                        loadChildren: '../account/account.module#AccountPageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/tabs/home',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
