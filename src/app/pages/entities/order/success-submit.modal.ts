import { Component, Input } from '@angular/core';
import {ModalController, NavController, NavParams, Platform, ToastController} from '@ionic/angular';

@Component({
    selector: 'success-submit-modal',
    templateUrl: 'success-submit.modal.html',
    styleUrls: ['success-submit.modal.scss']
})
export class SuccessSubmitModal {

    constructor(
        protected toastCtrl: ToastController,
        protected navController: NavController,
        public platform: Platform,
        private modalCtrl: ModalController,
    ) {
    }

    ionViewDidEnter() {
        console.log('did load');
    }

    previousState() {
        window.history.back();
    }

    closeModal() {
        this.navController.navigateForward('/tabs/checkout');
        this.modalCtrl.dismiss({
            'dismissed': true
        });
    }

}