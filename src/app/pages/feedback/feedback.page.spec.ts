import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FeedbackPage} from "./feedback.page";

describe('FeedbackPage', () => {
    let component: FeedbackPage;
    let fixture: ComponentFixture<FeedbackPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FeedbackPage],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [TranslateModule.forRoot(), RouterTestingModule, NgxWebstorageModule.forRoot(), HttpClientTestingModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FeedbackPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
