import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import {Menu} from "../../pages/entities/menu";
import {Category} from "../../pages/entities/category";

@Injectable({ providedIn: 'root'})
export class OpenApiService {
    private resourceUrl = ApiService.API_URL.replace('api', 'open-api');

    constructor(
        protected http: HttpClient,
        private apiService: ApiService,
        ) { }

    findMenu(id: number): Observable<HttpResponse<Menu>> {
        return this.http.get(`${this.resourceUrl}/menus/${id}`, { observe: 'response'});
    }

    findAllMenu(req?: any): Observable<any> {
        const options = createRequestOption(req);
        //return this.http.get<Menu[]>(this.resourceUrl, { params: options, observe: 'response' });
        return this.http.get(`${this.resourceUrl}/menus`, { params: options, observe: 'response' });
        ///return this.apiService.get(`${this.resourceUrl}/menus`, null, { params: options, observe: 'response' });
    }

    findAllCategories(req?: any): Observable<HttpResponse<Category[]>> {
        const options = createRequestOption(req);
        return this.http.get<Category[]>(`${this.resourceUrl}/categories`, { params: options, observe: 'response' });
    }

    sendSmsConfirmation(login: any, key: any): Observable<any> {
        return this.http.get(`${this.resourceUrl}/activate/sms?login=${login}&key=${key}`);
        ///return this.apiService.get(`${this.resourceUrl}/menus`, null, { params: options, observe: 'response' });
    }

    queryByIds(ids: number[]): Observable<HttpResponse<any[]>> {
        /*let options: HttpParams = new HttpParams();
        options = options.set('ids', ids);*/
        const params = ids.join(',');
        console.log('joined', params);

        return this.http.get<any[]>(`${this.resourceUrl}/favourites?id=${params}`, { observe: 'response' });
    }

    findAllAdverts(req?: any): Observable<any> {
        const options = createRequestOption(req);
        //return this.http.get<Menu[]>(this.resourceUrl, { params: options, observe: 'response' });
        return this.http.get(`${this.resourceUrl}/adverts`, { params: options, observe: 'response' });
        ///return this.apiService.get(`${this.resourceUrl}/menus`, null, { params: options, observe: 'response' });
    }
}
