import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { PriceHistory } from './price-history.model';

@Injectable({ providedIn: 'root'})
export class PriceHistoryService {
    private resourceUrl = ApiService.API_URL + '/price-histories';

    constructor(protected http: HttpClient) { }

    create(priceHistory: PriceHistory): Observable<HttpResponse<PriceHistory>> {
        return this.http.post<PriceHistory>(this.resourceUrl, priceHistory, { observe: 'response'});
    }

    update(priceHistory: PriceHistory): Observable<HttpResponse<PriceHistory>> {
        return this.http.put(this.resourceUrl, priceHistory, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<PriceHistory>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<PriceHistory[]>> {
        const options = createRequestOption(req);
        return this.http.get<PriceHistory[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
