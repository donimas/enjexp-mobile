import {Component, OnInit} from '@angular/core';
import {NavController, Platform, ToastController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from 'src/app/services/login/login.service';
import {UserService} from "../../services/user/user.service";
import {ApiService} from "../../services/api/api.service";
import {ClientApiService} from "../../services/client-api/client-api.service";

@Component({
    selector: 'feedback',
    templateUrl: './feedback.page.html',
    styleUrls: ['./feedback.page.scss']
})
export class FeedbackPage implements OnInit {

    feedback: any = {
        email: '',
        message: ''
    };

    constructor(
        public translateService: TranslateService,
        public toastController: ToastController,
        public platform: Platform,
        private apiService: ApiService,
        private clientApiService: ClientApiService,
        public navController: NavController
    ) {
    }

    ngOnInit() {
    }

    submit() {
        return this.apiService.post('feedbacks', this.feedback, { observe: 'response'}).subscribe((resp) => {
            this.clientApiService.showMsg('Спасибо за обратную связь! Мы ваши пожелания обязательно учтем');

            this.navController.back({animated: true});
        }, (err) => {
            this.clientApiService.showFLCError(err);
        });
    }
}
