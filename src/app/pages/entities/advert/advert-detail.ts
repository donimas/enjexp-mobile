import { Component, OnInit } from '@angular/core';
import { JhiDataUtils } from 'ng-jhipster';
import { Advert } from './advert.model';
import { AdvertService } from './advert.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-advert-detail',
    templateUrl: 'advert-detail.html'
})
export class AdvertDetailPage implements OnInit {
    advert: Advert = {};

    constructor(
        private dataUtils: JhiDataUtils,
        private navController: NavController,
        private advertService: AdvertService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.advert = response.data;
        });
    }

    open(item: Advert) {
        this.navController.navigateForward('/tabs/entities/advert/' + item.id + '/edit');
    }

    async deleteModal(item: Advert) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.advertService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/advert');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

}
