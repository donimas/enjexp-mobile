import {Component} from '@angular/core';
import {Events, ModalController, NavController} from "@ionic/angular";
import {UserService} from "../../services/user/user.service";
import {ContactInfoModal} from "../entities/order";
import {SmsConfirmModal} from "../entities/modals/sms-confirm.modal";
import {AccountService} from "../../services/auth/account.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage {

    checkoutAmount: number = 0;

    constructor(
        private events: Events,
        private userService: UserService,
        private navController: NavController,
        private accountService: AccountService,
        private router: Router,
        private modalController: ModalController
    ) {
        this.events.subscribe('checkout:amount', (res) => {
            console.log('checkout:number', res);
            let checkoutItems = this.userService.getCheckoutItems();
            console.log(checkoutItems);
            if(checkoutItems) {
                this.checkoutAmount = checkoutItems.length;
            }
        });
    }

    ionTabsDidChange($ev) {
        console.log('tab changed ----------->', $ev);
        const tab = $ev.tab;
        if(tab === 'checkout') {
            this.navController.navigateRoot('/tabs/'+tab);
        } else if (tab === 'account' && !this.accountService.isAuthenticated()) {
            this.navController.navigateForward('/signup', {animated: true});
        }
    }

}
