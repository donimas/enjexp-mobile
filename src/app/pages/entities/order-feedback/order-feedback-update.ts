import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { OrderFeedback } from './order-feedback.model';
import { OrderFeedbackService } from './order-feedback.service';
import { Order, OrderService } from '../order';
import { RatingTag, RatingTagService } from '../rating-tag';

@Component({
    selector: 'page-order-feedback-update',
    templateUrl: 'order-feedback-update.html'
})
export class OrderFeedbackUpdatePage implements OnInit {

    orderFeedback: OrderFeedback;
    orders: Order[];
    ratingTags: RatingTag[];
    createDate: string;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        rating: [null, []],
        comment: [null, []],
        createDate: [null, []],
        order: [null, []],
          ratingTags: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private orderService: OrderService,
        private ratingTagService: RatingTagService,
        private orderFeedbackService: OrderFeedbackService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.orderService.query()
            .subscribe(data => { this.orders = data.body; }, (error) => this.onError(error));
        this.ratingTagService.query()
            .subscribe(data => { this.ratingTags = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.orderFeedback = response.data;
            this.isNew = this.orderFeedback.id === null || this.orderFeedback.id === undefined;
        });
    }

    updateForm(orderFeedback: OrderFeedback) {
        this.form.patchValue({
            id: orderFeedback.id,
            rating: orderFeedback.rating,
            comment: orderFeedback.comment,
            createDate: (this.isNew) ? new Date().toISOString() : orderFeedback.createDate,
            order: orderFeedback.order,
            ratingTags: orderFeedback.ratingTags,
        });
    }

    save() {
        this.isSaving = true;
        const orderFeedback = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.orderFeedbackService.update(orderFeedback));
        } else {
            this.subscribeToSaveResponse(this.orderFeedbackService.create(orderFeedback));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<OrderFeedback>>) {
        result.subscribe((res: HttpResponse<OrderFeedback>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `OrderFeedback ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/order-feedback');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): OrderFeedback {
        return {
            ...new OrderFeedback(),
            id: this.form.get(['id']).value,
            rating: this.form.get(['rating']).value,
            comment: this.form.get(['comment']).value,
            createDate: new Date(this.form.get(['createDate']).value),
            order: this.form.get(['order']).value,
            ratingTags: this.form.get(['ratingTags']).value,
        };
    }

    compareOrder(first: Order, second: Order): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackOrderById(index: number, item: Order) {
        return item.id;
    }
    compareRatingTag(first: RatingTag, second: RatingTag): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackRatingTagById(index: number, item: RatingTag) {
        return item.id;
    }
}
