import { BaseEntity } from 'src/model/base-entity';
import { User } from '../../../services/user/user.model';
import { OrderStatusHistory } from '../order-status-history/order-status-history.model';
import { Location } from '../location/location.model';

export const enum PaymentType {
    'CASH',
    'EPAY',
    'KASPI'
}

export const enum ServiceType {
    'PICK_UP',
    'DELIVERY'
}

export const enum LocationType {
    'KAZGYU',
    'COMMUNITY',
    'CITY'
}

export class Order implements BaseEntity {
    constructor(
        public id?: number,
        public promocode?: string,
        public totalPrice?: number,
        public change?: number,
        public paid?: number,
        public paymentType?: PaymentType,
        public paymentCode?: string,
        public flagPaid?: boolean,
        public serviceType?: ServiceType,
        public serviceTime?: any,
        public createDate?: any,
        public comment?: string,
        public locationType?: LocationType,
        public deliveryDuration?: number,
        public user?: User,
        public status?: OrderStatusHistory,
        public location?: Location,
    ) {
        this.flagPaid = false;
    }
}
