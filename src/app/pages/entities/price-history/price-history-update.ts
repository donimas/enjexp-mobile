import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PriceHistory } from './price-history.model';
import { PriceHistoryService } from './price-history.service';
import { Menu, MenuService } from '../menu';

@Component({
    selector: 'page-price-history-update',
    templateUrl: 'price-history-update.html'
})
export class PriceHistoryUpdatePage implements OnInit {

    priceHistory: PriceHistory;
    menus: Menu[];
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        price: [null, [Validators.required]],
        currency: [null, []],
        menu: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private menuService: MenuService,
        private priceHistoryService: PriceHistoryService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.menuService.query()
            .subscribe(data => { this.menus = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.priceHistory = response.data;
            this.isNew = this.priceHistory.id === null || this.priceHistory.id === undefined;
        });
    }

    updateForm(priceHistory: PriceHistory) {
        this.form.patchValue({
            id: priceHistory.id,
            price: priceHistory.price,
            currency: priceHistory.currency,
            menu: priceHistory.menu,
        });
    }

    save() {
        this.isSaving = true;
        const priceHistory = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.priceHistoryService.update(priceHistory));
        } else {
            this.subscribeToSaveResponse(this.priceHistoryService.create(priceHistory));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<PriceHistory>>) {
        result.subscribe((res: HttpResponse<PriceHistory>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `PriceHistory ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/price-history');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): PriceHistory {
        return {
            ...new PriceHistory(),
            id: this.form.get(['id']).value,
            price: this.form.get(['price']).value,
            currency: this.form.get(['currency']).value,
            menu: this.form.get(['menu']).value,
        };
    }

    compareMenu(first: Menu, second: Menu): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackMenuById(index: number, item: Menu) {
        return item.id;
    }
}
