import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { JhiDataUtils } from 'ng-jhipster';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Menu } from './menu.model';
import { MenuService } from './menu.service';
import { Category, CategoryService } from '../category';
import { PriceHistory, PriceHistoryService } from '../price-history';

@Component({
    selector: 'page-menu-update',
    templateUrl: 'menu-update.html'
})
export class MenuUpdatePage implements OnInit {

    menu: Menu;
    categories: Category[];
    priceHistories: PriceHistory[];
    menus: Menu[];
    //@ViewChild('fileInput') fileInput;
    cameraOptions: CameraOptions;
    createDate: string;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        name: [null, []],
        description: [null, []],
        image: [null, []],
        imageContentType: [null, []],
        createDate: [null, []],
        flagDeleted: ['false', []],
        content: [null, []],
        prepDuration: [null, []],
        category: [null, []],
        price: [null, []],
        combo: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private dataUtils: JhiDataUtils,

        private elementRef: ElementRef,
        private camera: Camera,
        private categoryService: CategoryService,
        private priceHistoryService: PriceHistoryService,
        private menuService: MenuService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

        // Set the Camera options
        this.cameraOptions = {
            quality: 100,
            targetWidth: 900,
            targetHeight: 600,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            allowEdit: true,
            sourceType: 1
        };
    }

    ngOnInit() {
        this.categoryService.query()
            .subscribe(data => { this.categories = data.body; }, (error) => this.onError(error));
        this.priceHistoryService.query()
            .subscribe(data => { this.priceHistories = data.body; }, (error) => this.onError(error));
        this.menuService.query()
            .subscribe(data => { this.menus = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.menu = response.data;
            this.isNew = this.menu.id === null || this.menu.id === undefined;
        });
    }

    updateForm(menu: Menu) {
        this.form.patchValue({
            id: menu.id,
            name: menu.name,
            description: menu.description,
            image: menu.image,
            imageContentType: menu.imageContentType,
            createDate: (this.isNew) ? new Date().toISOString() : menu.createDate,
            flagDeleted: menu.flagDeleted,
            content: menu.content,
            prepDuration: menu.prepDuration,
            category: menu.category,
            price: menu.price,
            combo: menu.combo,
        });
    }

    save() {
        this.isSaving = true;
        const menu = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.menuService.update(menu));
        } else {
            this.subscribeToSaveResponse(this.menuService.create(menu));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Menu>>) {
        result.subscribe((res: HttpResponse<Menu>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `Menu ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/menu');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): Menu {
        return {
            ...new Menu(),
            id: this.form.get(['id']).value,
            name: this.form.get(['name']).value,
            description: this.form.get(['description']).value,
            image: this.form.get(['image']).value,
            imageContentType: this.form.get(['imageContentType']).value,
            createDate: new Date(this.form.get(['createDate']).value),
            flagDeleted: this.form.get(['flagDeleted']).value,
            content: this.form.get(['content']).value,
            prepDuration: this.form.get(['prepDuration']).value,
            category: this.form.get(['category']).value,
            price: this.form.get(['price']).value,
            combo: this.form.get(['combo']).value,
        };
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
        this.processWebImage(event, field);
    }

    getPicture(fieldName) {
        if (Camera.installed()) {
            this.camera.getPicture(this.cameraOptions).then((data) => {
                this.menu[fieldName] = data;
                this.menu[fieldName + 'ContentType'] = 'image/jpeg';
                this.form.patchValue({ [fieldName]: data });
                this.form.patchValue({ [fieldName + 'ContentType']: 'image/jpeg' });
            }, (err) => {
                alert('Unable to take photo');
            });
        } else {
            //this.fileInput.nativeElement.click();
            console.log('because of conflict');
        }
    }

    processWebImage(event, fieldName) {
        const reader = new FileReader();
        reader.onload = (readerEvent) => {

            let imageData = (readerEvent.target as any).result;
            const imageType = event.target.files[0].type;
            imageData = imageData.substring(imageData.indexOf(',') + 1);

            this.form.patchValue({ [fieldName]: imageData });
            this.form.patchValue({ [fieldName + 'ContentType']: imageType });
        };

        reader.readAsDataURL(event.target.files[0]);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.menu, this.elementRef, field, fieldContentType, idInput);
        this.form.patchValue( {[field]: ''} );
    }
    compareCategory(first: Category, second: Category): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackCategoryById(index: number, item: Category) {
        return item.id;
    }
    comparePriceHistory(first: PriceHistory, second: PriceHistory): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackPriceHistoryById(index: number, item: PriceHistory) {
        return item.id;
    }
    compareMenu(first: Menu, second: Menu): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackMenuById(index: number, item: Menu) {
        return item.id;
    }
}
