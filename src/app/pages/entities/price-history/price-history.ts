import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { PriceHistory } from './price-history.model';
import { PriceHistoryService } from './price-history.service';

@Component({
    selector: 'page-price-history',
    templateUrl: 'price-history.html'
})
export class PriceHistoryPage {
    priceHistories: PriceHistory[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private priceHistoryService: PriceHistoryService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.priceHistories = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.priceHistoryService.query().pipe(
            filter((res: HttpResponse<PriceHistory[]>) => res.ok),
            map((res: HttpResponse<PriceHistory[]>) => res.body)
        )
        .subscribe(
            (response: PriceHistory[]) => {
                this.priceHistories = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: PriceHistory) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/price-history/new');
    }

    edit(item: IonItemSliding, priceHistory: PriceHistory) {
        this.navController.navigateForward('/tabs/entities/price-history/' + priceHistory.id + '/edit');
        item.close();
    }

    async delete(priceHistory) {
        this.priceHistoryService.delete(priceHistory.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'PriceHistory deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(priceHistory: PriceHistory) {
        this.navController.navigateForward('/tabs/entities/price-history/' + priceHistory.id + '/view');
    }
}
