import {HttpClient, HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AuthExpiredInterceptor} from './interceptors/auth-expired.interceptor';
import {AuthInterceptor} from './interceptors/auth.interceptor';
import {PinDialogOriginal} from "@ionic-native/pin-dialog";
import {PinDialog} from "@ionic-native/pin-dialog/ngx";
import {SmsConfirmModal} from "./pages/entities/modals/sms-confirm.modal";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ContactInfoModal} from "./pages/entities/order";
import {StockDetailPage} from "./pages/entities/menu/stock-detail.page";
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import {PhoneMaskDirective} from "./services/client-api/phone-mask.directive";
import {OneSignal} from "@ionic-native/onesignal/ngx";

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        //ContactInfoModal,
        SmsConfirmModal,
        StockDetailPage,
        PhoneMaskDirective,
    ],
    entryComponents: [
        //ContactInfoModal,
        SmsConfirmModal,
        StockDetailPage,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        IonicModule.forRoot(),
        NgxWebstorageModule.forRoot({prefix: 'jhi', separator: '-'}),
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule
    ],
    providers: [
        PinDialog,
        StatusBar,
        SplashScreen,
        SocialSharing,
        PhoneMaskDirective,
        OneSignal,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
