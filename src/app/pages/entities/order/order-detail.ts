import { Component, OnInit } from '@angular/core';
import {Order, PaymentType, ServiceType} from './order.model';
import { OrderService } from './order.service';
import {NavController, AlertController, ModalController, Events} from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import {ContactInfoModal} from "./contact-info.modal";
import {SuccessSubmitModal} from "./success-submit.modal";
import {UserService} from "../../../services/user/user.service";
import {SmsConfirmModal} from "../modals/sms-confirm.modal";
import {AccountService} from "../../../services/auth/account.service";
import {ClientApiService} from "../../../services/client-api/client-api.service";
import {OrderStatus} from "../order-status-history";

@Component({
    selector: 'page-order-detail',
    templateUrl: 'order-detail.html',
    styleUrls: ['order.page.scss']
})
export class OrderDetailPage implements OnInit {
    order: any = null;
    checkoutItems: any[];
    pickUpTime: Date;
    account: any = null;
    promocode: string;
    comment: string;
    flagComment: boolean;
    notCanceledStatuses: OrderStatus[];
    promocodeConfirm: any;

    subtotal: number;
    total: number;

    constructor(
        private navController: NavController,
        private orderService: OrderService,
        private activatedRoute: ActivatedRoute,
        private modalController: ModalController,
        private userService: UserService,
        private accountService: AccountService,
        private clientApiService: ClientApiService,
        private events: Events,
        private alertController: AlertController
    ) {
        this.events.subscribe('account:refresh', () => {
            this.setAccount();
        });
    }

    ngOnInit(): void {
        this.checkoutItems = this.userService.getCheckoutItems();
        this.pickUpTime = this.userService.getPickupTime();
        console.log(this.checkoutItems);
        console.log(this.pickUpTime);
        this.promocodeConfirm = null;
        this.promocode = null;
        this.activatedRoute.data.subscribe((response) => {
            this.order = response.data;
            this.comment = this.order.comment;
            console.log('order',this.order);
            if(this.order && this.order.items) {
                this.checkoutItems = this.order.items;
                this.promocode = this.order.promocode;
                this.subtotal = this.order.subtotal;
                this.total = this.order.total;
                if(this.order.discount) {
                    this.promocodeConfirm = {
                        'name': 'Скидка',
                        'discount': this.order.discount
                    };
                }
                this.buildStatuses();
            } else {
                this.order = null;
            }
        });
        this.setAccount();
        //this.checkPromocodeOwner();
        if(!this.order || !this.order.id) {
            this.calcSubtotal(this.checkoutItems);
            this.checkStudentDiscount();
        }
    }

    calcSubtotal(items) {
        this.subtotal = this.orderService.calculate(this.checkoutItems);
    }

    checkStudentDiscount() {
        this.promocodeConfirm = {
            'name': 'Скидка студ.',
            'discount': 20
        };
        this.initTotalSum(this.subtotal, this.promocodeConfirm);
    }

    initTotalSum(subtotal, promoConfirm) {
        //subtotal - 100
        //x - 100-discount
        if(!promoConfirm) {
            return subtotal;
        }
        this.total = Math.round(subtotal * (100-promoConfirm.discount) / 100);
    }

    checkPromocodeOwner() {
        this.orderService.checkPromocodeOwner().subscribe((resp) => {
            console.log(resp);
            this.promocodeConfirm = resp;
            if(this.promocodeConfirm) {
                this.clientApiService.showMsg('Спасибо что поделились промо кодом! Мы Вам дарим 30% скидку.');
            }
        }, (err) => {
            console.log(err);
        });
    }

    buildStatuses() {
        this.notCanceledStatuses = [
            OrderStatus.IN_PROCESS,
            OrderStatus.DELIVERING,
            OrderStatus.FINISHED,
            OrderStatus.REJECTED
        ];
    }

    setAccount() {
        this.accountService.identity().then(account => {
            console.log('account is found', account);
            this.account = account;
        });
    }

    open(item: Order) {
        this.navController.navigateForward('/tabs/entities/order/' + item.id + '/edit');
    }

    async deleteModal(item: Order) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.orderService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/order');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }

    async presentPaymentAlert() {
        const alert = await this.alertController.create({
            header: 'В процессе',
            message: 'Вид оплаты по карте в процессе разработки.',
            buttons: ['OK']
        });

        await alert.present();
    }

    async makeOrder() {
        const alert = await this.alertController.create({
            header: 'Подтверждение',
            message: 'Подтвердите Ваш заказ',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Подтверждаю',
                    handler: () => {
                        console.log('Confirm Okay');
                        this.successSubmit();
                    }
                }
            ]
        });

        await alert.present();
    }

    async successSubmit() {
        let items = [];
        this.checkoutItems.forEach((item) => {
            const i = {
                amount: item.amount,
                spicyLevel: item.spicyLevel,
                menu: {
                    id: item.menu.id
                }
            };
            items.push(i);
        });
        console.log('created items', items);
        let req = {
            pickupTime: this.pickUpTime,
            login: this.account.login,
            promocode: this.promocode,
            paymentType: PaymentType.CASH,
            subtotal: this.subtotal,
            discount: this.promocodeConfirm ? this.promocodeConfirm.discount : null,
            total: this.total,
            comment: this.comment,
            serviceType: ServiceType.PICK_UP,
            items: items
        };
        console.log('request ->', req);

        this.orderService.clientCheckout(req).subscribe((resp) => this.onCheckoutSaveSuccess(resp),
            (err) => {
            console.log(err);
        });
    }

    async onCheckoutSaveSuccess(resp) {
        this.clearCheckout();
        console.log(resp);
        const modal = await this.modalController.create({
            component: SuccessSubmitModal,
            showBackdrop: true
        });
        await modal.present();
    }

    clearCheckout() {
        this.userService.setCheckoutItems(null);
    }

    showCommentInput() {
        console.log('show input');
        this.flagComment = !this.flagComment;
        //const el = document.querySelector('.hidden-comment-input');
        /*el.style.setProperty('opacity', '1');
        el.style.setProperty('height', 'auto');
        el.style.setProperty('transition', 'opacity 1s ease-out');*/
    }

    getPromocode(code: any) {
        console.log(code);
        if(!code || code.trim() === '') {
            console.log('code is empty');
            return;
        }
        this.orderService.getPromocode(code).subscribe((resp) => {
            console.log(resp);
            this.promocodeConfirm = resp;
            this.initTotalSum(this.subtotal, this.promocodeConfirm);
            this.clientApiService.showMsg('Спасибо что воспользовались нашиим приложением. ' +
                'По промо коду мы дарим Вам 30% скидку.');
        }, (err) => {
            console.log(err);
            this.clientApiService.showFLCError(err);
        });
    }

    async showCancelAlert(order: any) {
        const alert = await this.alertController.create({
            header: 'Внимание',
            message: 'Вы действительно хотите удалить заказ?',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Да',
                    handler: () => {
                        console.log('Confirm Okay');
                        this.cancelOrder(order.id);
                    }
                }
            ]
        });

        await alert.present();
    }

    cancelOrder(id) {
        this.orderService.clientCancelOrder(id).subscribe((resp) => {
            console.log(resp);
            this.navController.navigateForward('/tabs/checkout');
            this.clientApiService.showMsg("Заказ успешно отменен");
        }, (err) => {
            console.log(err);
            this.clientApiService.showFLCError(err);
        });
    }

    hasAnyStatus(order: any) {
        if(!order || !order.statusHistory) {
            return;
        }
        const currentStatus = order.statusHistory.orderStatus;
        return this.notCanceledStatuses.indexOf(currentStatus) !== -1;
    }

}
