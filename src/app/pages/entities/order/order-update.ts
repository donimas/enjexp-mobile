import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Order } from './order.model';
import { OrderService } from './order.service';
import { User } from '../../../services/user/user.model';
import { UserService } from '../../../services/user/user.service';
import { OrderStatusHistory, OrderStatusHistoryService } from '../order-status-history';
import { Location, LocationService } from '../location';

@Component({
    selector: 'page-order-update',
    templateUrl: 'order-update.html'
})
export class OrderUpdatePage implements OnInit {

    order: Order;
    users: User[];
    orderStatusHistories: OrderStatusHistory[];
    locations: Location[];
    serviceTime: string;
    createDate: string;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        promocode: [null, []],
        totalPrice: [null, []],
        change: [null, []],
        paid: [null, []],
        paymentType: [null, []],
        paymentCode: [null, []],
        flagPaid: ['false', []],
        serviceType: [null, []],
        serviceTime: [null, []],
        createDate: [null, []],
        comment: [null, []],
        locationType: [null, []],
        deliveryDuration: [null, []],
        user: [null, []],
        status: [null, []],
        location: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private userService: UserService,
        private orderStatusHistoryService: OrderStatusHistoryService,
        private locationService: LocationService,
        private orderService: OrderService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.userService.findAll().subscribe(data => this.users = data, (error) => this.onError(error));
        this.orderStatusHistoryService.query()
            .subscribe(data => { this.orderStatusHistories = data.body; }, (error) => this.onError(error));
        this.locationService.query()
            .subscribe(data => { this.locations = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.order = response.data;
            this.isNew = this.order.id === null || this.order.id === undefined;
        });
    }

    updateForm(order: Order) {
        this.form.patchValue({
            id: order.id,
            promocode: order.promocode,
            totalPrice: order.totalPrice,
            change: order.change,
            paid: order.paid,
            paymentType: order.paymentType,
            paymentCode: order.paymentCode,
            flagPaid: order.flagPaid,
            serviceType: order.serviceType,
            serviceTime: (this.isNew) ? new Date().toISOString() : order.serviceTime,
            createDate: (this.isNew) ? new Date().toISOString() : order.createDate,
            comment: order.comment,
            locationType: order.locationType,
            deliveryDuration: order.deliveryDuration,
            user: order.user,
            status: order.status,
            location: order.location,
        });
    }

    save() {
        this.isSaving = true;
        const order = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.orderService.update(order));
        } else {
            this.subscribeToSaveResponse(this.orderService.create(order));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Order>>) {
        result.subscribe((res: HttpResponse<Order>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `Order ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/order');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): Order {
        return {
            ...new Order(),
            id: this.form.get(['id']).value,
            promocode: this.form.get(['promocode']).value,
            totalPrice: this.form.get(['totalPrice']).value,
            change: this.form.get(['change']).value,
            paid: this.form.get(['paid']).value,
            paymentType: this.form.get(['paymentType']).value,
            paymentCode: this.form.get(['paymentCode']).value,
            flagPaid: this.form.get(['flagPaid']).value,
            serviceType: this.form.get(['serviceType']).value,
            serviceTime: new Date(this.form.get(['serviceTime']).value),
            createDate: new Date(this.form.get(['createDate']).value),
            comment: this.form.get(['comment']).value,
            locationType: this.form.get(['locationType']).value,
            deliveryDuration: this.form.get(['deliveryDuration']).value,
            user: this.form.get(['user']).value,
            status: this.form.get(['status']).value,
            location: this.form.get(['location']).value,
        };
    }

    compareUser(first: User, second: User): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
    compareOrderStatusHistory(first: OrderStatusHistory, second: OrderStatusHistory): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackOrderStatusHistoryById(index: number, item: OrderStatusHistory) {
        return item.id;
    }
    compareLocation(first: Location, second: Location): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackLocationById(index: number, item: Location) {
        return item.id;
    }
}
