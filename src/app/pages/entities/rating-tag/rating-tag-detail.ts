import { Component, OnInit } from '@angular/core';
import { RatingTag } from './rating-tag.model';
import { RatingTagService } from './rating-tag.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-rating-tag-detail',
    templateUrl: 'rating-tag-detail.html'
})
export class RatingTagDetailPage implements OnInit {
    ratingTag: RatingTag = {};

    constructor(
        private navController: NavController,
        private ratingTagService: RatingTagService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.ratingTag = response.data;
        });
    }

    open(item: RatingTag) {
        this.navController.navigateForward('/tabs/entities/rating-tag/' + item.id + '/edit');
    }

    async deleteModal(item: RatingTag) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.ratingTagService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/rating-tag');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
