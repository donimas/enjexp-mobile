import { BaseEntity } from 'src/model/base-entity';
import { Order } from '../order/order.model';
import { Menu } from '../menu/menu.model';

export const enum OrderItemType {
    'PAY',
    'GIFT'
}

export class OrderItem implements BaseEntity {
    constructor(
        public id?: number,
        public amount?: number,
        public type?: OrderItemType,
        public order?: Order,
        public menu?: Menu,
    ) {
    }
}
