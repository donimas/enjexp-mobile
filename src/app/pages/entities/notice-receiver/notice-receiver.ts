import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { NoticeReceiver } from './notice-receiver.model';
import { NoticeReceiverService } from './notice-receiver.service';

@Component({
    selector: 'page-notice-receiver',
    templateUrl: 'notice-receiver.html'
})
export class NoticeReceiverPage {
    noticeReceivers: NoticeReceiver[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private noticeReceiverService: NoticeReceiverService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.noticeReceivers = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.noticeReceiverService.query().pipe(
            filter((res: HttpResponse<NoticeReceiver[]>) => res.ok),
            map((res: HttpResponse<NoticeReceiver[]>) => res.body)
        )
        .subscribe(
            (response: NoticeReceiver[]) => {
                this.noticeReceivers = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: NoticeReceiver) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/notice-receiver/new');
    }

    edit(item: IonItemSliding, noticeReceiver: NoticeReceiver) {
        this.navController.navigateForward('/tabs/entities/notice-receiver/' + noticeReceiver.id + '/edit');
        item.close();
    }

    async delete(noticeReceiver) {
        this.noticeReceiverService.delete(noticeReceiver.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'NoticeReceiver deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(noticeReceiver: NoticeReceiver) {
        this.navController.navigateForward('/tabs/entities/notice-receiver/' + noticeReceiver.id + '/view');
    }
}
