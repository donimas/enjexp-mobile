import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { RatingTag } from './rating-tag.model';
import { RatingTagService } from './rating-tag.service';

@Component({
    selector: 'page-rating-tag-update',
    templateUrl: 'rating-tag-update.html'
})
export class RatingTagUpdatePage implements OnInit {

    ratingTag: RatingTag;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        name: [null, []],
        eq: [null, []],
        code: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private ratingTagService: RatingTagService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.ratingTag = response.data;
            this.isNew = this.ratingTag.id === null || this.ratingTag.id === undefined;
        });
    }

    updateForm(ratingTag: RatingTag) {
        this.form.patchValue({
            id: ratingTag.id,
            name: ratingTag.name,
            eq: ratingTag.eq,
            code: ratingTag.code,
        });
    }

    save() {
        this.isSaving = true;
        const ratingTag = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.ratingTagService.update(ratingTag));
        } else {
            this.subscribeToSaveResponse(this.ratingTagService.create(ratingTag));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<RatingTag>>) {
        result.subscribe((res: HttpResponse<RatingTag>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `RatingTag ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/rating-tag');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): RatingTag {
        return {
            ...new RatingTag(),
            id: this.form.get(['id']).value,
            name: this.form.get(['name']).value,
            eq: this.form.get(['eq']).value,
            code: this.form.get(['code']).value,
        };
    }

}
