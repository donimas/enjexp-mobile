import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { Camera } from '@ionic-native/camera/ngx';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { AdvertPage } from './advert';
import { AdvertUpdatePage } from './advert-update';
import { Advert, AdvertService, AdvertDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class AdvertResolve implements Resolve<Advert> {
  constructor(private service: AdvertService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Advert> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Advert>) => response.ok),
        map((advert: HttpResponse<Advert>) => advert.body)
      );
    }
    return of(new Advert());
  }
}

const routes: Routes = [
    {
      path: '',
      component: AdvertPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: AdvertUpdatePage,
      resolve: {
        data: AdvertResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: AdvertDetailPage,
      resolve: {
        data: AdvertResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: AdvertUpdatePage,
      resolve: {
        data: AdvertResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        AdvertPage,
        AdvertUpdatePage,
        AdvertDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ],
    providers: [Camera]
})
export class AdvertPageModule {
}
