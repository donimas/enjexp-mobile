import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { RatingTag } from './rating-tag.model';

@Injectable({ providedIn: 'root'})
export class RatingTagService {
    private resourceUrl = ApiService.API_URL + '/rating-tags';

    constructor(protected http: HttpClient) { }

    create(ratingTag: RatingTag): Observable<HttpResponse<RatingTag>> {
        return this.http.post<RatingTag>(this.resourceUrl, ratingTag, { observe: 'response'});
    }

    update(ratingTag: RatingTag): Observable<HttpResponse<RatingTag>> {
        return this.http.put(this.resourceUrl, ratingTag, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<RatingTag>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<RatingTag[]>> {
        const options = createRequestOption(req);
        return this.http.get<RatingTag[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
